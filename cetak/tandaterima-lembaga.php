<?php
#$base_url = '/rokeu-banpem/web/cetak/';
$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];
#echo $_SERVER['PATH_INFO'].'<hr />';

$path = explode('/', $_path);
#var_dump($path);
#echo '<hr />';

if (is_numeric($path[1])) {
  #  echo 'angka nih';
  #require '/var/www/html/rokeu-banpem/web/barcode/vendor/autoload.php';
  require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  #define ('DRUPAL_ROOT', '/var/www/html/rokeu-banpem/web');
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $_proposal = node_load($path[1]);

  if (!$_proposal) {
    die('Invalid Node ID');
  }

  #var_dump($_proposal); die();
  #jika perseorangan
  if ($_proposal->field_pro_jenis_proposal['und'][0]['tid'] == 494) {
    #echo 'perseorangan<hr />';
    $_submitter = $_proposal->field_pro_perseorangan['und'][0]['target_id'];
  } elseif ($_proposal->field_pro_jenis_proposal['und'][0]['tid'] == 495) {
    #echo 'Lembaga<hr />';
    $_submitter = $_proposal->field_pro_lembaga['und'][0]['target_id'];
  }
  #die ($_submitter);
  $submitter = node_load($_submitter);
  #var_dump($submitter);die();
  if (!$submitter) {
    die('Invalid Submitter Node ID');
  }

} else {
  die('Error parameter!');
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pencetakan Tanda Terima</title>

  </head>
  <body>

<table width="650">
<tr>
  <td width="120px;"><img src="<?php echo $base_url; ?>images/kemdikbud_v2.png" /></td>
  <td align="center">
    <div style="font-size: large;">Kementerian Pendidikan dan Kebudayaan</div>
    <div style="font-size: x-small;">Kompleks Kementerian Pendidikan dan Kebudayaan</div>
    <div style="font-size: x-small;">Jalan Jenderal Sudirman, Senayan Jakarta Pusat 10270</div>
    <div style="font-size: x-small;">Call Center: 177 SMS: 0811976929</div>
  </td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td colspan="2" align="center" style="font-size: small; font-weight: bold; line-height: 1.2em;">
    <div>Kartu Tanda Kelengkapan Berkas</div>
    <div>Nomor Pendaftaran: <?php echo $_proposal->field_pro_nomor_pendaftaran['und'][0]['safe_value']; ?></div>
  </td>
</tr>
<tr>
  <td colspan="2" style="font-size: small; line-height: 1em;">
    Telah diterima bantuan proposal pemerintah tahun 2018 dari:
  </td>
</tr>
</table>
<table style="font-size: small;" width="650">
  <tr style="line-height: 0.8em;">
    <td>Kode Lembaga</td>
    <td>:</td>
    <td><?php echo $_submitter; ?></td>
  </tr>
  <tr style="line-height: 0.8em;">
    <td>Nama Lembaga</td>
    <td>:</td>
    <td><?php echo $submitter->title; ?></td>
  </tr>
  <tr style="line-height: 0.8em;">
    <td valign="top">Alamat</td>
    <td valign="top">:</td>
    <td style="line-height: 1.2em;"><?php echo preg_replace("/<p>|<\/p>/i", "", $submitter->field_lem_alamat['und'][0]['safe_value']); ?></td>
  </tr>
  <tr style="line-height: 0.8em;">
    <td>No. Telp.</td>
    <td>:</td>
    <td><?php echo $submitter->field_lem_telepon['und'][0]['value']; ?></td>
  </tr>
  <tr style="line-height: 0.8em;">
    <td>Email</td>
    <td>:</td>
    <td><?php echo $submitter->field_lem_email['und'][0]['value']; ?></td>
  </tr>
  <tr style="">
    <td valign="top">Kelengkapan berkas</td>
    <td valign="top">:</td>
    <td style="line-height: 1em;">
      <?php #print views_embed_view('kelengkapan_berkas_cetak_tanda_terima_v2', 'page_a', $path[1]); ?>
      <?php #print views_embed_view('kelengkapan_berkas_cetak_tanda_terima_v2', 'page_b', $path[1]); ?>
      <div>- Surat Pengantar</div>
      <div>- NSS/Ijin operasional/NILEK</div>
      <div>- RAB</div>
      <div>- FC Rekening</div>
      <div>- Nomor Telepon</div>
    </td>
  </tr>
  <tr>
    <td colspan="3"><hr /></td>
  </tr>
</table>
<?php 
$__tgl_daftar = explode(" ", $_proposal->field_pro_tanggal_pendaftaran['und'][0]['value']);
$_tgl_daftar = explode("-", $__tgl_daftar[0]);
?>

<table style="font-size: small;">
  <tr style="line-height: 1em;">
    <td width="200">Pemohon<br /><br /><br /><br /><br />........................</td>
    <td width="200">Penerima<br /><br /><br /><br /><br />........................</td>
    <td width="250" valign="top">Jakarta, <?php echo $_tgl_daftar[2].' - '.$_tgl_daftar[1].' - '.$_tgl_daftar[0]; ?></td>
  </tr>
  <tr>
    <td colspan="3"><hr /></td>
  </tr>
  <tr style="line-height: 1em;">
    <td width="200"></td>
    <td width="200">Verifikasi<span style="font-size: large;"> &#x2610;</span></td>
    <td width="250">Administrasi<span style="font-size: large;"> &#x2610;</span></td>
  </tr>

</table>
<table width="650">
<tr>
  <td width="120px;"><img src="<?php echo $base_url; ?>images/kemdikbud_v2.png" /></td>
  <td align="center">
    <div style="font-size: large;">Kementerian Pendidikan dan Kebudayaan</div>
    <div style="font-size: x-small;">Kompleks Kementerian Pendidikan dan Kebudayaan</div>
    <div style="font-size: x-small;">Jalan Jenderal Sudirman, Senayan Jakarta Pusat 10270</div>
    <div style="font-size: x-small;">Call Center: 177 SMS: 0811976929</div>
  </td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<table width="650" style="font-size: small;">
  <tr style="line-height: 0.9em;">
    <td>Nomor Pendaftaran</td>
    <td>:</td>
    <td><?php echo $_proposal->field_pro_nomor_pendaftaran['und'][0]['safe_value']; ?></td>
  </tr>
  <tr style="line-height: 0.9em;">
    <td>Kode Lembaga</td>
    <td>:</td>
    <td><?php echo $_submitter; ?></td>
  </tr>
  <tr style="line-height: 0.9em;">
    <td>Nama Lembaga</td>
    <td>:</td>
    <td><?php echo $submitter->title; ?></td>
  </tr>
  <tr>
    <td valign="top">Alamat</td>
    <td valign="top">:</td>
    <td><?php echo preg_replace("/<p>|<\/p>/i", "", $submitter->field_lem_alamat['und'][0]['safe_value']); ?></td>
  </tr>
  <tr style="line-height: 0.9em;">
    <td>No. Telp.</td>
    <td>:</td>
    <td><?php echo $submitter->field_lem_telepon['und'][0]['value']; ?></td>
  </tr>
  <tr style="line-height: 1em;">
    <td>Email</td>
    <td>:</td>
    <td><?php echo $submitter->field_lem_email['und'][0]['value']; ?></td>
  </tr>
  <tr>
    <td colspan="3">
              <?php
              $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
              echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($_proposal->field_pro_nomor_pendaftaran['und'][0]['safe_value'], $generator::TYPE_CODE_128, 2, 50)) . '">';
              ?>


    </td>
  </tr>
  <tr style="line-height: 0.3em;">
    <td colspan="3">
<?php echo $_proposal->field_pro_nomor_pendaftaran['und'][0]['safe_value']; ?>
    </td>
  </tr>

  <tr>
    <td colspan="3">
<hr />
    </td>
  </tr>


</table>






  </body>
</html>
