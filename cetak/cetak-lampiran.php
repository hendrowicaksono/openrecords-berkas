<?php

$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];

$path = explode('/', $_path);

if (is_numeric($path[1])) {
  require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $_sk = node_load($path[1]);
  $_jenis_bantuan = taxonomy_term_load ($_sk->field_sk_jenis_bantuan['und'][0]['tid']);
  $_total = number_format($_sk->field_sk_total['und'][0]['value']);

  if (!$_sk) {
    die('Invalid Node ID');
  }
  if ($_sk->type != 'sk') {
    die('Invalid Node Type');
  }

  #cek apakah status sudah menjadi SK
  if ($_sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Not ready for SK printing.');
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'proposal')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('field_sk', 'target_id', $path[1], '=')
    ->fieldOrderBy('field_pro_nomor_pendaftaran', 'value', 'DESC')
    #->range(0, 10)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));

  $result = $query->execute();
  if (isset($result['node'])) {
    #echo count($result['node']); die();
    $total_rec = count($result['node']);

    $counter = 0;
    foreach ($result['node'] as $k => $v) {
      $counter++;

      if ( ($counter == 1) OR ($counter % 5 == 1) ) {
        #echo "<h1>Mulai TABLE</h1>";
        echo "<table border='1'>\n";
        echo "<tr>\n";
        echo "<td width=\"5%\">No.</td>\n";
        echo "<td width=\"35%\">NAMA DAN ALAMAT LEMBAGA / ORGANISASI</td>\n";
        echo "<td width=\"20%\">NAMA BANK DAN NO REKENING</td>\n";
        echo "<td width=\"20%\">NAMA PENERIMA</td>\n";
        echo "<td width=\"20%\">JUMLAH PERSETUJUAN</td>\n";
        echo "</tr>\n";
      }

      $prop = node_load($k);
      #echo 'Nomor: '.$counter."<br />\n";
      echo "<tr>\n";
      echo '<td>'.$counter."</td>\n";
      #echo 'NID Jenis Proposal: '.$prop->field_pro_jenis_proposal['und'][0]['tid']."<br />\n";
      #jika proposal perseorangan
      if ($prop->field_pro_jenis_proposal['und'][0]['tid'] === '494') {
        #echo 'NID Perseorangan: '.$prop->field_pro_perseorangan['und'][0]['target_id']."<br />\n";
        $instansi = node_load($prop->field_pro_perseorangan['und'][0]['target_id']);
        $nama_alamat = $instansi->title.'<br />'.nl2br($instansi->field_inv_alamat['und'][0]['value']);
        #echo 'Nama dan Alamat: '.$nama_alamat."<br />\n";
        echo '<td>'.$nama_alamat."</td>";
        #echo 'NID Bank: '.$instansi->field_inv_bank_v2['und'][0]['target_id']."<br />\n";
        $_bank = taxonomy_term_load($instansi->field_inv_bank_v2['und'][0]['target_id']);
        $bank = $_bank->name;
        $bank_dan_rekening = $bank.'<br />'.$instansi->field_inv_cabang_bank['und'][0]['value'].'<br />'.$instansi->field_inv_no_rekening['und'][0]['value'];
        #echo 'Nama Bank dan No. Rekening: '.$bank_dan_rekening."<br />\n";
        echo '<td>'.$bank_dan_rekening."</td>";
        #echo 'NAMA PENERIMA: '.$instansi->title."<br />\n";
        echo '<td>'.$instansi->title."</td>";
      } else {
        #echo 'NID Lembaga: '.$prop->field_pro_lembaga['und'][0]['target_id']."<br />\n";
        $instansi = node_load($prop->field_pro_lembaga['und'][0]['target_id']);
        $nama_alamat = $instansi->title.'<br />'.nl2br($instansi->field_lem_alamat['und'][0]['value']);
        #echo 'Nama dan Alamat: '.$nama_alamat."<br />\n";
        echo '<td>'.$nama_alamat."</td>";
        #echo 'NID Bank: '.$instansi->field_lem_bank_v2['und'][0]['target_id']."<br />\n";
        $_bank = taxonomy_term_load($instansi->field_lem_bank_v2['und'][0]['target_id']);
        $bank = $_bank->name;
        $bank_dan_rekening = $bank.'<br />'.$instansi->field_lem_cabang_bank['und'][0]['value'].'<br />'.$instansi->field_lem_no_rekening['und'][0]['value'];
        #echo 'Nama Bank dan No. Rekening: '.$bank_dan_rekening."<br />\n";
        echo '<td>'.$bank_dan_rekening."</td>";
        #echo 'NAMA PENERIMA: '.$instansi->title."<br />\n";
        echo '<td>'.$instansi->title."</td>";
      }
      #echo 'JUMLAH PERSETUJUAN: '.$prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']."<br />\n";
      echo '<td>'.$instansi->title."</td>";
      #echo "<br />\n";
      echo "</tr>\n";

      if ( ($counter % 5) == 0) {
        echo "</table>\n";
      }

      if ($counter  == $total_rec) {
        echo "</table>\n";
        echo "<h1>Signature disini</h1>\n";
      }


    }

  } 

} else {
  die('Error parameter!');
}
?>
