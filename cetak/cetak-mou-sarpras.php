<?php
#$base_url = '/rokeu-banpem/web/cetak/';
$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];
#echo $_SERVER['PATH_INFO'].'<hr />';

$path = explode('/', $_path);
#var_dump($path);
#echo '<hr />';

if (is_numeric($path[1])) {
  #require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);



$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'proposal')
  ->propertyCondition('nid', $path[1])
  ->propertyCondition('status', NODE_PUBLISHED)
  ->fieldCondition('field_sk', 'target_id', 'NULL', '!=')

  #->fieldCondition('field_news_types', 'value', 'spotlight', '=')
  // See the comment about != NULL above.
  #->fieldCondition('field_faculty_tag', 'tid', $value)
  #->fieldCondition('field_news_publishdate', 'value', db_like($year) . '%', 'like') // Equal to "starts with"
  #->fieldCondition('field_news_subtitle', 'value', '%' . db_like($year) . '%', 'like') // Equal to "contains"
  #->fieldOrderBy('field_photo', 'fid', 'DESC')
  ->range(0, 1)
  // Run the query as user 1.
  ->addMetaData('account', user_load(1));

$result = $query->execute();
if (isset($result['node'])) {
  $_prop_nid = array_keys($result['node']);
  $prop_nid = $_prop_nid[0];
  $prop = node_load($prop_nid);

  if (!$prop) {
    die('Invalid Node ID (1)');
  }

  if ($prop->type != 'proposal') {
    die('Invalid Node Type (1)');
  }

  #var_dump($prop->field_sk['und'][0]['target_id']); die();
  $sk = node_load($prop->field_sk['und'][0]['target_id']);

  if (!$sk) {
    die('Invalid Node ID (2)');
  }

  if ($sk->type != 'sk') {
    #var_dump($sk);
    die('Invalid Node Type (2)');
  }

  #cek apakah status sudah menjadi SK
  if ($sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Not ready for SK printing.');
  }

  #make sure this is only for jenis bantuan SARPRAS
  # tid = 1106,
  #var_dump($sk->field_sk_jenis_bantuan['und'][0]['tid']); die(); 
  if ($sk->field_sk_jenis_bantuan['und'][0]['tid'] != '1104') {
    die('Not allowed.');
  }

  $_tahun_anggaran = taxonomy_term_load ($sk->field_sk_tahun_anggaran['und'][0]['tid']);
  $tahun_anggaran = $_tahun_anggaran->name;

  #jika jenis proposal 495 (Lembaga)
  if ($prop->field_pro_jenis_proposal['und'][0]['tid'] == '495') {
    #var_dump($prop->field_pro_lembaga['und'][0]['target_id']);
    $instansi = node_load($prop->field_pro_lembaga['und'][0]['target_id']);
    #var_dump($instansi);
    #echo taxonomy_term_load($prop->field_pro_jenis_proposal['und'][0]['tid'])->name;
    #die();
    $nama_bank = taxonomy_term_load($instansi->field_lem_bank_v2['und'][0]['target_id'])->name.', '.$instansi->field_lem_cabang_bank['und'][0]['value'];
    $no_rekening = $instansi->field_lem_no_rekening['und'][0]['value'];
    $nama_rekening = $instansi->field_lem_atas_nama['und'][0]['value'];
    $penanggungjawab = $instansi->field_lem_kontak['und'][0]['value'];
    $alamat = $instansi->field_lem_alamat['und'][0]['value'];
  } else {
    $instansi = node_load($prop->field_pro_perseorangan['und'][0]['target_id']);
    $nama_bank = taxonomy_term_load($instansi->field_inv_bank_v2['und'][0]['target_id'])->name.', '.$instansi->field_inv_cabang_bank['und'][0]['value'];
    $no_rekening = $instansi->field_inv_no_rekening['und'][0]['value'];
    $nama_rekening = $instansi->title;
    $penanggungjawab = $instansi->title;
    $alamat = $instansi->field_inv_alamat['und'][0]['value'];
  }


  #echo ($sk->field_sk_tanggal_dibuat['und'][0]['value'])."<br />";
  #echo strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value'])."<br  />";
  #echo strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value'] + 3600)."<br  />";
  #echo date("d m y", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']))."<br  />";
  #echo date("d m Y", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400)."<br  />";
  $tanggal = date("d", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  $bulan = date("m", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  $tahun = date("Y", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  $tanggal_mou = $tanggal.' '.month2text($bulan).' '.$tahun;
  $hari = date("D", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  #echo date("D", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 604800);
  #echo $tanggal_mou;
  #var_dump($prop->field_pro_jenis_proposal['und'][0]['tid']);
  #echo taxonomy_term_load($prop->field_pro_jenis_proposal['und'][0]['tid'])->name;
  #var_dump($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']);
  #die();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SK Pemberian Bantuan Pemerintah</title>
  </head>
  <body>

<table width="700">
<tr>
  <td width="120px;"><img src="<?php echo $base_url; ?>images/kemdikbud_v2.png" /></td>
  <td align="center">
    <div style="font-size: X-large;">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</div>
    <div style="">Jalan Jenderal Sudirman Senayan, Jakarta 10270</div>
    <div style="">Telp. (021) 5711144 (Hunting)</div>
    <div style="">Laman: www.kemdikbud.go.id</div>
  </td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>

<table width="700">
<tr>
<td align="center">
PERJANJIAN KERJASAMA<br />
PEMBERIAN BANTUAN SARANA PRASARANA<br /><br />
TAHUN ANGGARAN <?php echo $tahun_anggaran; ?>
<br /><br />ANTARA<br /><br /> 
PEJABAT PEMBUAT KOMITMEN BIRO KEUANGAN <br />
SEKRETARIAT JENDERAL KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN
<br /><br />DENGAN <br /><br /><?php echo $instansi->title; ?>
</td>
</tr>
</table><br />
<table width="700">
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>NOMOR</td>
  <td> : </td>
  <td><?php echo $sk->title; ?>/A2.1/KU/2018</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>TANGGAL</td>
  <td> : </td>
  <td><?php echo $tanggal_mou; ?></td>
</tr>
</table>
<br />
<table width="700">
<tr>
<td align="justify">
Pada hari ini <?php echo day2text($hari); ?> tanggal <?php echo date2text($tanggal); ?> 
bulan <?php echo month2text($bulan); ?> tahun <?php echo terbilang($tahun); ?> 
telah diadakan Perjanjian Kerjasama Pemberian Bantuan Pemerintah pada Biro Keuangan Sekretariat Jenderal Kementerian Pendidikan dan Kebudayaan, antara :

</td>
</tr>
</table>
<br />
<table width="700" border="0">
  <tr>
    <td valign="top">1.</td>
    <td>
      <table border="0">
        <tr>
          <td>Nama </td>
          <td> : </td>
          <td>Taopiq, S.Pd., M.M</td>
        </tr>
        <tr>
          <td>NIP </td>
          <td> : </td>
          <td>197703112000031002</td>
        </tr>
        <tr>
          <td valign="top">Jabatan </td>
          <td valign="top"> : </td>
          <td>Kepala Sub Bagian Pembiayaan<br />Biro Keuangan Sekretariat Jenderal</td>
        </tr>
        <tr>
          <td>Alamat </td>
          <td> : </td>
          <td>Jl. Jenderal Sudirman Senayan  Jakarta</td>
        </tr>
        <tr>
          <td colspan="3">Bertindak untuk dan atas nama Biro Keuangan Sekretariat Jenderal Kementerian Pendidikan dan Kebudayaan selaku Pejabat Pembuat Komitmen (PPK), yang disebut PIHAK PERTAMA.</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top">2.</td>
    <td>
      <table border="0">
        <tr>
          <td>Nama </td>
          <td> : </td>
          <td><?php echo $instansi->field_lem_kontak['und'][0]['value']; ?></td>
        </tr>
        <tr>
          <td>Pimpinan/Ketua</td>
          <td> : </td>
          <td><?php echo $instansi->title; ?></td>
        </tr>
        <tr>
          <td valign="top">Alamat </td>
          <td valign="top"> : </td>
          <td><?php echo $alamat; ?></td>
        </tr>
        <tr>
          <td colspan="3">Bertindak untuk dan atas nama (nama lembaga/organisasi), yang selanjutnya disebut PIHAK KEDUA.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br />
<table width="700">
<tr>
<td align="center">
Pasal 1<br />
Ruang Lingkup Perjanjian Kerjasama<br /><br />
</td>
</tr>
<tr>
<td align="justify">
PIHAK PERTAMA mengadakan perjanjian dengan PIHAK KEDUA berupa pemberian bantuan pemerintah pada Biro Keuangan Setjen Kemendikbud. 
</td>
</tr>
<tr>
<td align="center">
<br />Pasal 2<br />
Tanggung Jawab<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>PIHAK KEDUA bertanggungjawab terhadap pelaksanaan penggunaan dana bantuan yang diterima dari PIHAK PERTAMA;</li>
<li>Apabila terjadi penyalahgunaan terhadap penggunaan dana bantuan yang diterima dari PIHAK PERTAMA maka PIHAK KEDUA bertanggungjawab terhadap konsekuensi hukum yang berlaku.</li>
</ol>
</td>
</tr>
<tr>
<td align="center">
Pasal 3<br />
Hak dan Kewajiban PIHAK PERTAMA<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
  <li>PIHAK PERTAMA berhak:
    <ol type="a">
      <li>Menetapkan lembaga/organisasi penerima bantuan;</li>
      <li>Menetapkan jumlah dana bantuan;</li>
      <li>Menerima laporan pertanggungjawaban penggunaan dana dan pelaksanaan bantuan dari PIHAK KEDUA sesuai ketentuan.</li>
    </ol>
  </li>
  <li>PIHAK PERTAMA berkewajiban:
    <ol type="a">
      <li>Melakukan pengecekan kelengkapan data dan verifikasi terhadap kelengkapan persyaratan proposal permohonan;</li>
      <li>Menyalurkan dana bantuan kepada PIHAK KEDUA melalui Bank penyalur sesuai dengan ketentuan;</li>
      <li>Melakukan monitoring dan evaluasi pelaksanaan bantuan oleh PIHAK KEDUA pada kondisi tertentu;</li>
      <li>Meminta laporan pelaksanaan pekerjaan yang dilakukan oleh PIHAK KEDUA;</li>
      <li>Memberikan teguran dan atau sanksi kepada PIHAK KEDUA, baik secara lisan maupun tertulis, apabila dalam pelaksanaan kegiatan dan penggunaan dana bantuan tersebut tidak sesuai dengan surat perjanjian kerjasama.</li>
    </ol>
  </li>
</ol>
</td>
</tr>
<tr>
<td align="center">
Pasal 4<br />
Hak dan Kewajiban PIHAK KEDUA<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
  <li>PIHAK KEDUA berhak:
    <ol type="a">
      <li>Menerima dana bantuan dari PIHAK PERTAMA sesuai dengan surat perjanjian kerjasama;</li>
      <li>Menggunakan dana bantuan sesuai dengan petunjuk teknis penyaluran bantuan pemerintah dan RAB yang disepakati;</li>
    </ol>
  </li>
  <li>PIHAK KEDUA berkewajiban:
    <ol type="a">
      <li>Melaksanakan pekerjaan sesuai dengan yang telah disepakati dalam Perjanjian Kerja Sama antara PIHAK PERTAMA dan PIHAK KEDUA;</li>
      <li>Mempertanggungjawabkan penggunaan dana bantuan yang telah diterima sesuai dengan ketentuan dan peraturan yang berlaku dan tidak memberikan imbalan dalam bentuk apapun kepada siapapun yang terkait dengan penerimaan dana bantuan;</li>
      <li>Menyusun dan menyampaikan laporan pertanggungjawaban penggunaan dana dan pelaksanaan pekerjaan bantuan kepada PIHAK PERTAMA;</li>
      <li>Bertanggungjawab sepenuhnya terhadap segala bentuk penyimpangan, penyalahgunaan, dan pelanggaran penggunaan dana sesuai dengan peraturan dan perundang-undangan yang berlaku;</li>
      <li>Mentaati teguran/peringatan/sanksi yang disampaikan oleh PIHAK PERTAMA, baik secara lisan maupun tertulis.</li>
    </ol>
  </li>
</ol>
</td>
</tr>
<tr>
<td align="center">
Pasal 5<br />
Jenis Pekerjaan<br /><br />
</td>
</tr>
<tr>
<td align="justify">
PIHAK KEDUA menerima dana bantuan pemerintah dari PIHAK PERTAMA untuk melaksanakan pekerjaan sesuai dengan proposal yang diajukan berupa bantuan untuk sarana prasarana.
</td>
</tr>
<tr>
<td align="center">
<br />Pasal 6<br />
Nilai dan Rincian Dana Bantuan<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>Nilai dana bantuan yang diberikan PIHAK PERTAMA kepada PIHAK KEDUA sebesar Rp.<?php echo number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, '.', '.'); ?>,- terbilang (<?php echo terbilang($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']).' rupiah'; ?>).</li>
<li>Nilai bantuan sebagaimana dimaksud pada ayat (1) digunakan untuk bantuan sarana prasarana.</li>
</ol>
</td>
</tr>
<tr>
<td align="center">
<br />Pasal 7<br />
Jangka Waktu Penyelesaian Pekerjaan<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>Jangka waktu penyelesaian pekerjaan selama 30 (tiga puluh) hari kalender terhitung sejak dana diterima;</li>
<li>Jangka waktu pelaksanaan pekerjaan dapat diperpanjang atas persetujuan PIHAK PERTAMA, didasarkan pada surat permohonan perpanjangan dari PIHAK KEDUA dengan alasan yang dapat dipertanggungjawabkan.</li>
</ol>
</td>
</tr>
<tr>
<td align="center">
<br />Pasal 8<br />
Penyaluran Dana Bantuan<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>Penyaluran dana bantuan akan dilakukan setelah semua persyaratan dipenuhi dan surat perjanjian kerjasama ditandatangani  oleh PIHAK PERTAMA dan PIHAK KEDUA;</li>
<li>Penyaluran  dana bantuan pada ayat (1), dilakukan melalui Proses pemindahbukuan secara langsung melalui Bank BNI KLN Kemendikbud ke rekening PIHAK KEDUA:
  <table>
    <tr>
      <td>Nama Bank</td>
      <td> : </td>
      <td> <?php echo $nama_bank; ?> </td>
    </tr>
    <tr>
      <td>No Rekening</td>
      <td> : </td>
      <td> <?php echo $no_rekening; ?></td>
    </tr>
    <tr>
      <td>Nama Rekening</td>
      <td> : </td>
      <td> <?php echo $nama_rekening; ?></td>
    </tr>
  </table>
</li>
</ol>
</td>
</tr>
<tr>
<td align="center">
<br />Pasal 9<br />
Sanksi<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>Apabila berdasarkan hasil monitoring dan evaluasi yang dilakukan oleh PIHAK PERTAMA atau temuan aparat pengawas, ternyata PIHAK KEDUA terbukti melakukan kekeliruan/kesalahan dalam melaksanakan kegiatan/program yang telah disepakati, maka PIHAK PERTAMA akan menyampaikan teguran baik secara lisan maupun tertulis kepada PIHAK KEDUA;</li>
<li>Teguran PIHAK PERTAMA kepada PIHAK KEDUA berisi permintaan untuk memperbaiki/menyelesaikan segala bentuk kesalahan/kekeliruan yang telah dilakukan;</li>
<li>Apabila PIHAK KEDUA terbukti menggunakan dana tidak sesuai sebagaimana yang diatur dalam perjanjian kerja sama ini dan/atau digunakan untuk kepentingan pribadi, maka penerima bantuan wajib mengembalikan dana bantuan yang telah diterima ke Kas Negara;</li>
<li>Pengembalian bantuan sebagaimana dimaksud pada ayat (3) dilakukan PIHAK KEDUA melalui Bank BNI cabang setempat dengan:
  <ol type="a">
    <li>Mengisi SSPB (Surat Setoran Pengembalian Belanja) apabila dalam tahun anggaran berjalan dengan kode MAP (disesuaikan dengan kode akun pengeluaran);</li>
    <li>Mengisi SSBP (Surat Setoran Bukan Pajak) apabila tahun anggaran berikutnya dengan kode MAP 423958.</li>
  </ol>
</li>
<li>Apabila PIHAK KEDUA tidak mampu mengembalikan dana bantuan sebagaimana dimaksud pada ayat (3) di atas, maka PIHAK KEDUA tidak akan diberikan bantuan lagi oleh PIHAK PERTAMA pada tahun-tahun berikutnya.</li>
</ol>
</td>
</tr>

<tr>
<td align="center">
<br />Pasal 10<br />
Pelaporan dan Pertanggungjawaban<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>PIHAK KEDUA wajib menyusun dan menyampaikan laporan pertanggungjawaban kepada PIHAK PERTAMA paling lambat 30 (tiga puluh) hari setelah pekerjaan selesai;</li>
<li>Laporan pertanggungjawaban sesuai yang disebutkan pada ayat (1) tersebut harus dilampiri:
  <ol type="a">
    <li>Laporan Pertanggungjawaban Bantuan Operasional (khusus untuk bantuan operasional); </li>
    <li>Berita Acara Serah Terima (khusus untuk bantuan sarana/prasarana,  bantuan rehabilitasi/pembangunan gedung/bangunan, dan bantuan lainnya yang ditetapkan oleh PA);</li>
    <li>Bukti surat setoran sisa dana (apabila ada);</li>
    <li>Dokumentasi/foto kegiatan atau barang yang dihasilkan/dibeli;</li>
  </ol>
</li>
<li>Bukti-bukti yang sah (kuitansi pengeluaran bermaterai, pembelian material, dan bukti penyetoran pajak (bila ada), serta bukti-bukti lainnya disimpan oleh PIHAK KEDUA sebagai dokumen pemeriksaan.</li>
</ol>
</td>
</tr>

<tr>
<td align="center">
Pasal 11<br />
Penanggungan Resiko<br /><br />
</td>
</tr>
<tr>
<td align="justify">
PIHAK KEDUA berkewajiban untuk membebaskan dan menanggung tanpa batas PIHAK PERTAMA beserta instansinya terhadap akibat yang timbul atas semua konsekuensi hukum dan biaya sehubungan dengan ditandatanganinya perjanjian ini.
</td>
</tr>

<tr>
<td align="center">
<br />Pasal 12<br />
Keadaan Memaksa (<i>Force Majeure</i>)<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>Yang dimaksud keadaan memaksa (Force Majeure) adalah peristiwa seperti: bencana alam (gempa bumi, tanah longsor, banjir), kebakaran, perang, huru-hara, pemogokkan, pemberontakan, dan epidemi yang secara keseluruhan ada hubungan langsung dengan penyelesaian pekerjaan;</li>
<li>Apabila terjadi keadaan Force Majeure sebagaimana dimaksud pada ayat (1) di atas, maka kedua belah pihak setuju untuk merevisi surat perjanjian dan pelaksanaan pekerjaan.</li>
</ol>
</td>
</tr>

<tr>
<td align="center">
<br />Pasal 13<br />
Lain-lain<br />
</td>
</tr>
<tr>
<td align="justify">
<ol style="padding-left: 20px;">
<li>Surat Perjanjian ini dianggap sah setelah ditandatangani oleh kedua belah pihak;</li>
<li>Biaya materai dalam Surat Perjanjian Kerjasama ini dibebankan kepada PIHAK KEDUA;</li>
<li>Perubahan atas Surat Perjanjian Kerjasama ini dapat dilakukan atas persetujuan kedua belah pihak;</li>
<li>Surat Perjanjian ini dibuat rangkap 2 (dua), lembar pertama dan kedua masing-masing dibubuhi materai Rp6.000,- (enam ribu rupiah);</li>
<li>Dokumen ini beserta lampirannya merupakan bagian yang tidak terpisahkan dari surat perjanjian kerjasama.</li>
</ol>
</td>
</tr>
</table><br />

<table width="700">
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
<td>Jakarta, <?php echo $tanggal_mou; ?></td></tr>
</table>
<br />
<table width="700">
<tr>
  <td valign="top" align="center">
    PIHAK PERTAMA<br />
Pejabat Pembuat Komitmen,<br />
Bagian Perbendaharaan dan Pembiayaan
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
</td>
  <td valign="top" align="center">
PIHAK KEDUA<br />
<?php echo $instansi->title; ?>
  </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
  <td valign="top" align="center">
    Taopiq, S.Pd, M.M
</td>
  <td valign="top" align="center">
<?php echo $penanggungjawab; ?>
  </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>


<table width="700">
<tr>
<td style="font-size: smaller;">
<br />&nbsp;
<br />&nbsp;
<br />&nbsp;
<br />&nbsp;
<br />&nbsp;
<br />&nbsp;
<br />&nbsp;
<br />&nbsp;
*) Dibuat rangkap 2 (dua),<br />
1 (satu) untuk disimpan oleh PIHAK PERTAMA (bermaterai),<br />
1 (satu) untuk disimpan oleh PIHAK KEDUA (bermaterai)</td>
</tr>
</table>

<?php

} else {
  die('data tidak tersedia');
}










  #die('oke disini dulu');
} else {
  die('Error parameter!');
}
?>
