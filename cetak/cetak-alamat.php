<?php
#$base_url = '/rokeu-banpem/web/cetak/';
$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];
#echo $_SERVER['PATH_INFO'].'<hr />';

$path = explode('/', $_path);
#var_dump($path);
#echo '<hr />';

if (is_numeric($path[1])) {
  #require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  $sk = node_load($path[1]);
  #var_dump($sk->field_sk_status['und'][0]['tid']);die();


  if ($sk->type != 'sk') {
    die('Maaf, bukan SK valid');
  }

  #die('here?');

  if ($sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Maaf, bukan SK valid (2)');
  }  


$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'proposal')
  #->propertyCondition('nid', $path[1])
  ->propertyCondition('status', NODE_PUBLISHED)
  ->fieldCondition('field_sk', 'target_id', $sk->nid, '=')

  #->fieldCondition('field_news_types', 'value', 'spotlight', '=')
  // See the comment about != NULL above.
  #->fieldCondition('field_faculty_tag', 'tid', $value)
  #->fieldCondition('field_news_publishdate', 'value', db_like($year) . '%', 'like') // Equal to "starts with"
  #->fieldCondition('field_news_subtitle', 'value', '%' . db_like($year) . '%', 'like') // Equal to "contains"
  #->fieldOrderBy('field_photo', 'fid', 'DESC')
  #->range(0, 1)
  // Run the query as user 1.
  ->addMetaData('account', user_load(1));

$result = $query->execute();

if (isset($result['node'])) {
  foreach ($result['node'] as $k => $v) {
    $prop = node_load($k);
    #echo $prop->field_pro_jenis_proposal['und'][0]['tid'].'<hr />';

    if ($prop->field_pro_jenis_proposal['und'][0]['tid'] == '495') {
      $instansi = node_load($prop->field_pro_lembaga['und'][0]['target_id']);
      $nama_instansi = $instansi->title;
      $penanggungjawab = $instansi->field_lem_kontak['und'][0]['value'];
      $nama_dituju = '<div>Kepada Yth. <span style="font-weight: bold; font-size: large;">'.$nama_instansi.'</span></div>';
      $nama_dituju .= '<div>'.$penanggungjawab.'</div>';
      $alamat = '<div>'.nl2br($instansi->field_lem_alamat['und'][0]['value']).'</div>';
      $telepon = '<div>Telp. '.$instansi->field_lem_telepon['und'][0]['value'].'</div>';
    } else {
      $instansi = node_load($prop->field_pro_perseorangan['und'][0]['target_id']);
      $penanggungjawab = $$instansi->title;
      $nama_dituju = '<div>Kepada Yth. <span style="font-weight: bold; font-size: large;">'.$penanggungjawab.'</span></div>';
      $alamat = '<div>'.nl2br($instansi->field_inv_alamat['und'][0]['value']).'</div>';
      $telepon = '<div>Telp. '.$instansi->field_inv_telepon['und'][0]['value'].'</div>';
    }

    echo $nama_dituju."\n";
    echo $alamat."\n";
    echo $telepon."\n";
    echo "<br /><br />\n";

  }

} else {
  die('data tidak tersedia');
}

}
?>