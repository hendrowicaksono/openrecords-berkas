<?php
#$base_url = '/rokeu-banpem/web/cetak/';
$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];
#echo $_SERVER['PATH_INFO'].'<hr />';

$path = explode('/', $_path);
#var_dump($path);
#echo '<hr />';

if (is_numeric($path[1])) {
  require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $_sk = node_load($path[1]);
  #var_dump($_sk->field_sk_jenis_bantuan['und'][0]['tid']); die();
  $_jenis_bantuan = taxonomy_term_load ($_sk->field_sk_jenis_bantuan['und'][0]['tid']);
  #var_dump($_jenis_bantuan->name); die();
  #var_dump($_sk->field_sk_total['und'][0]['value']); die();
  $_total = number_format($_sk->field_sk_total['und'][0]['value']);
  #die($_total);

  if (!$_sk) {
    die('Invalid Node ID');
  }
  if ($_sk->type != 'sk') {
    die('Invalid Node Type');
  }

  #cek apakah status sudah menjadi SK
  if ($_sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Not ready for SK printing.');
  }

  $header_sk_01 = node_load(25394);
  $header_sk_02 = node_load(25557);
  $sk_intro_membaca = node_load(25564);
  $sk_intro_menimbang = node_load(25565);
  $sk_intro_mengingat = node_load(25570);

  $__tanggal = explode(" ", $_sk->field_sk_tanggal_dibuat['und'][0]['value']);
  $_tanggal = explode("-", $__tanggal[0]);
  #var_dump($_tanggal); die();
  $tanggal = $_tanggal[2].' '.month2text($_tanggal[1]).' '.$_tanggal[0];
  #echo ($tanggal); die();

  #die('oke disini dulu');
} else {
  die('Error parameter!');
}
?>
<style type="text/css">
.header_sk_01 {
  font-weight: bold;
  text-align: center;
}
.sk_intro {
  text-align: justify;
}
.sk_intro ol {
  margin-left: 0px;
  padding-left: 12px;
}
.memutuskan {
  text-align: center;
}
</style>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SK Pemberian Bantuan Pemerintah</title>
  </head>
  <body>

<table width="700">
<tr>
  <td width="120px;"><img src="<?php echo $base_url; ?>images/kemdikbud_v2.png" /></td>
  <td align="center">
    <div style="font-size: X-large;">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</div>
    <div style="">Jalan Jenderal Sudirman Senayan, Jakarta 10270</div>
    <div style="">Telp. (021) 5711144 (Hunting)</div>
    <div style="">Laman: www.kemdikbud.go.id</div>
  </td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>

<table width="700">
<tr>
  <td class="header_sk_01">
    <?php echo nl2br($header_sk_01->field_longtext['und'][0]['value']); ?><br />
    NOMOR: <?php echo $_sk->title; ?>/A2.1/KU/2018<br />
    <?php echo nl2br($header_sk_02->field_longtext['und'][0]['value']); ?>
  </td>
</tr>
</table>
&nbsp;<br />
<table width="700" class="sk_intro">
<tr>
  <td valign="top">Membaca
  </td>
  <td valign="top"> : 
  </td>
  <td>
    <?php echo ($sk_intro_membaca->field_longtext['und'][0]['value']); ?>
  </td>
</tr>
<tr>
  <td valign="top">Menimbang
  </td>
  <td valign="top"> : 
  </td>
  <td>
    <?php echo ($sk_intro_menimbang->field_longtext['und'][0]['value']); ?>
  </td>
</tr>
<tr>
  <td valign="top">Mengingat
  </td>
  <td valign="top"> : 
  </td>
  <td>
    <?php echo ($sk_intro_mengingat->field_longtext['und'][0]['value']); ?>
  </td>
</tr>
</table>

<table width="700" class="sk_intro">
<tr>
  <td class="memutuskan">MEMUTUSKAN :</td>
</tr>
</table>

<span style="">MENETAPKAN</span>
<table width="700" class="sk_intro">
<tr>
  <td valign="top">PERTAMA
  </td>
  <td valign="top"> : 
  </td>
  <td>
    Memberikan bantuan pemerintah kepada:
    <table class="sk_intro">
      <tr>
        <td>Kepada</td>
        <td> : </td>
        <td>Daftar Terlampir</td>
      </tr>
      <tr>
        <td>Alamat</td>
        <td> : </td>
        <td>Terlampir</td>
      </tr>
      <tr>
        <td>Jenis bantuan</td>
        <td> : </td>
        <td><?php echo $_jenis_bantuan->name; ?></td>
      </tr>
      <tr>
        <td>Jumlah Bantuan Sebesar</td>
        <td> : </td>
        <td>Rp <?php echo number_format($_sk->field_sk_total['und'][0]['value'], 0, ".", "."); ?> (<?php echo terbilang($_sk->field_sk_total['und'][0]['value']).' rupiah'; ?>)</td>
      </tr>
      <tr>
        <td>Nama Bank</td>
        <td> : </td>
        <td>Terlampir</td>
      </tr>
      <tr>
        <td>Nama Rekening</td>
        <td> : </td>
        <td>Terlampir</td>
      </tr>
      <tr>
        <td>Nomor Rekening</td>
        <td> : </td>
        <td>Terlampir</td>
      </tr>
      <tr>
        <td>Jenis Pembayaran</td>
        <td> : </td>
        <td>LS</td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td valign="top">KEDUA
  </td>
  <td valign="top"> : 
  </td>
  <td>
    Dana bantuan yang diberikan dibebankan pada DIPA Biro Keuangan Nomor : DIPA-023.01.1.690285/2018.
  </td>
</tr>
<tr>
  <td valign="top">KETIGA
  </td>
  <td valign="top"> : 
  </td>
  <td>
    Pemberian bantuan dilaksanakan dengan ketentuan sebagai berikut: 
    <ol>
      <li>Bantuan diberikan sesuai Petunjuk Teknis Bantuan Pemerintah pada Biro Keuangan Tahun 2018;</li>
      <li>Besaran dana bantuan ditetapkan berdasarkan hasil verifikasi Rincian Anggaran Biaya (RAB) yang disampaikan oleh lembaga/organisasi;</li>
      <li>Jenis bantuan yang diberikan dapat berupa Bantuan Operasional, Bantuan Sarana/Prasarana, Bantuan Rehabilitasi/Pembangunan Gedung/Bangunan, atau Bantuan Lainnya;</li>
      <li>Ketentuan tentang penggunaan dana bantuan dan sanksi-sanksi diatur dalam Perjanjian Kerjasama antara PPK Biro Keuangan dengan penerima bantuan;</li>
      <li>Setelah dana bantuan disalurkan dan diterima, maka tanggung jawab penggunaan dana sepenuhnya menjadi tanggung jawab penerima bantuan.</li>
   </ol>
  </td>
</tr>
<tr>
  <td valign="top">KEEMPAT
  </td>
  <td valign="top"> : 
  </td>
  <td>
    Jika dikemudian hari terdapat kekeliruan dalam Surat Keputusan ini, akan diadakan perubahan sebagaimana mestinya.
  </td>
</tr>
</table>
<table width="700" class="sk_intro">
<tr>
  <td width="50%">&nbsp;</td>
  <td>Ditetapkan di </td>
  <td> : </td>
  <td>Jakarta</td>
</tr>
<tr>
  <td></td>
  <td>Pada tanggal </td>
  <td> : </td>
  <td><?php echo $tanggal; ?></td>
</tr>
</table>
&nbsp;<br />
<table width="700" class="sk_intro">
<tr>
  <td width="50%">
    Disahkan<br />
    Kepala Biro Keuangan<br />
    Selaku Kuasa Pengguna Anggaran<br />
    &nbsp;<br />
    &nbsp;<br />
    &nbsp;<br />
    &nbsp;<br />
    <b>M.Q. Wisnu Aji</b><br />
    NIP. 196005221986011001
  </td>
  <td width="50%">
    &nbsp;<br />
    Pejabat Pembuat Komitmen<br />
    &nbsp;<br />
    &nbsp;<br />
    &nbsp;<br />
    &nbsp;<br />
    &nbsp;<br />
    <b>Taopiq</b><br />
    NIP. 197703112000031002
  </td>
</tr>
</table>

&nbsp;<br />
<table width="700" class="sk_intro">
<tr>
  <td>
Salinan keputusan ini disampaikan Kepada Yth:
<ol>
<li>Menteri Pendidikan dan Kebudayaan;</li>
<li>Sekretaris Jenderal Kementerian Pendidikan dan Kebudayaan;</li>
<li>Inspektur Jenderal Kementerian Pendidikan dan Kebudayaan;</li>
<li>Kepala KPPN Jakarta III;</li>
<li>Ybs.</li>
</ol>
  </td>
</tr>
</table>




  </body>
</html>
