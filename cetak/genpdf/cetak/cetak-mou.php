<?php
#$base_url = '/rokeu-banpem/web/cetak/';
$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];
#echo $_SERVER['PATH_INFO'].'<hr />';

$path = explode('/', $_path);
#var_dump($path);
#echo '<hr />';

if (is_numeric($path[1])) {
  #require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);



$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'proposal')
  ->propertyCondition('nid', $path[1])
  ->propertyCondition('status', NODE_PUBLISHED)
  ->fieldCondition('field_sk', 'target_id', 'NULL', '!=')

  #->fieldCondition('field_news_types', 'value', 'spotlight', '=')
  // See the comment about != NULL above.
  #->fieldCondition('field_faculty_tag', 'tid', $value)
  #->fieldCondition('field_news_publishdate', 'value', db_like($year) . '%', 'like') // Equal to "starts with"
  #->fieldCondition('field_news_subtitle', 'value', '%' . db_like($year) . '%', 'like') // Equal to "contains"
  #->fieldOrderBy('field_photo', 'fid', 'DESC')
  ->range(0, 1)
  // Run the query as user 1.
  ->addMetaData('account', user_load(1));

$result = $query->execute();
if (isset($result['node'])) {

  $_prop_nid = array_keys($result['node']);
  $prop_nid = $_prop_nid[0];
  $prop = node_load($prop_nid);

  if (!$prop) {
    die('Invalid Node ID (1)');
  }

  if ($prop->type != 'proposal') {
    die('Invalid Node Type (1)');
  }

  #var_dump($prop->field_sk['und'][0]['target_id']); die();
  $sk = node_load($prop->field_sk['und'][0]['target_id']);

  if (!$sk) {
    die('Invalid Node ID (2)');
  }

  if ($sk->type != 'sk') {
    #var_dump($sk);
    die('Invalid Node Type (2)');
  }

  #cek apakah status sudah menjadi SK
  if ($sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Not ready for SK printing.');
  }

  #make sure this is only for jenis bantuan
  # tid = 1103, 1104, 1105, 1106
  #if ($sk->field_sk_jenis_bantuan['und'][0]['tid'] != '1104') {
  #  die('Not allowed.');
  #}
  if (!(($sk->field_sk_jenis_bantuan['und'][0]['tid'] != '1103') OR 
  ($sk->field_sk_jenis_bantuan['und'][0]['tid'] != '1104') OR
  ($sk->field_sk_jenis_bantuan['und'][0]['tid'] != '1105') OR 
  ($sk->field_sk_jenis_bantuan['und'][0]['tid'] != '1106')))
  {
    die('Not allowed.');
  }

  $jenis_bantuan_sk = '';
  if ($sk->field_sk_jenis_bantuan['und'][0]['tid'] == 1103) {
    $jenis_bantuan_sk = 'Bantuan Operasional';
  } elseif ($sk->field_sk_jenis_bantuan['und'][0]['tid'] == 1104) {
    $jenis_bantuan_sk = 'Bantuan Sarana Prasarana';
  } elseif ($sk->field_sk_jenis_bantuan['und'][0]['tid'] == 1105) {
    $jenis_bantuan_sk = 'Bantuan Pembangunan / Rehabilitasi';
    $jenis_rehab = taxonomy_term_load($prop->field_pro_jenis_bantuan['und'][0]['tid'])->name;
    #$jenis_rehab = ($prop->field_pro_jenis_bantuan['und'][0]['target_id']);
    #die($prop->field_pro_jenis_bantuan['und'][0]['tid']);
    #$jenis_rehab = taxonomy_term_load($prop->field_pro_jenis_bantuan['und'][0]['target_id']);
  } elseif ($sk->field_sk_jenis_bantuan['und'][0]['tid'] == 1106) {
    $jenis_bantuan_sk = 'Bantuan Lainnya';
  }
  #die('<h1>'.$sk->field_sk_jenis_bantuan['und'][0]['tid'].'</h1>');

  $_tahun_anggaran = taxonomy_term_load ($sk->field_sk_tahun_anggaran['und'][0]['tid']);
  $tahun_anggaran = $_tahun_anggaran->name;

  #jika jenis proposal 495 (Lembaga)
  if ($prop->field_pro_jenis_proposal['und'][0]['tid'] == '495') {
    #var_dump($prop->field_pro_lembaga['und'][0]['target_id']);
    $instansi = node_load($prop->field_pro_lembaga['und'][0]['target_id']);
    #var_dump($instansi);
    #echo taxonomy_term_load($prop->field_pro_jenis_proposal['und'][0]['tid'])->name;
    #die();
    $nama_bank = taxonomy_term_load($instansi->field_lem_bank_v2['und'][0]['target_id'])->name;
    $cabang_bank = $instansi->field_lem_cabang_bank['und'][0]['value'];
    $no_rekening = $instansi->field_lem_no_rekening['und'][0]['value'];
    $nama_rekening = $instansi->field_lem_atas_nama['und'][0]['value'];
    $penanggungjawab = $instansi->field_lem_kontak['und'][0]['value'];
    $alamat = $instansi->field_lem_alamat['und'][0]['value'];
  } else {
    $instansi = node_load($prop->field_pro_perseorangan['und'][0]['target_id']);
    $nama_bank = taxonomy_term_load($instansi->field_inv_bank_v2['und'][0]['target_id'])->name;
    $cabang_bank = $instansi->field_inv_cabang_bank['und'][0]['value'];
    $no_rekening = $instansi->field_inv_no_rekening['und'][0]['value'];
    $nama_rekening = $instansi->title;
    $penanggungjawab = $instansi->title;
    $alamat = $instansi->field_inv_alamat['und'][0]['value'];
  }

#die('<h1>'.$nama_bank.'</h1>');

  #echo ($sk->field_sk_tanggal_dibuat['und'][0]['value'])."<br />";
  #echo strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value'])."<br  />";
  #echo strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value'] + 3600)."<br  />";
  #echo date("d m y", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']))."<br  />";
  #echo date("d m Y", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400)."<br  />";
  $tanggal = date("d", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  $bulan = date("m", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  $tahun = date("Y", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  $tanggal_mou = $tanggal.' '.month2text($bulan).' '.$tahun;
  $hari = date("D", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 86400);
  #echo date("D", strtotime($sk->field_sk_tanggal_dibuat['und'][0]['value']) + 604800);
  #echo $tanggal_mou;
  #var_dump($prop->field_pro_jenis_proposal['und'][0]['tid']);
  #echo taxonomy_term_load($prop->field_pro_jenis_proposal['und'][0]['tid'])->name;
  #var_dump($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']);
  #die();

  $kode_akun = '08119831038';

#############INCLUDE THE TCPDF##################################

require_once('tcpdf_include.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('mou-sarpas');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
#$pdf->setPrintFooter(true);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
#$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(PDF_MARGIN_LEFT, '15', PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
  require_once(dirname(__FILE__).'/lang/eng.php');
  $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 12);
$pdf->setCellHeightRatio(1.5);

$pdf->SetMargins(25, PDF_MARGIN_TOP, 23);

$pdf->AddPage('P', 'A4');
###############################################

$_page = '';

$_page .= '&nbsp;<br />';
$_page .= '<table>';
$_page .= '<tr>';
$_page .= '<td align="center">';
$_page .= 'PERJANJIAN KERJASAMA<br />';
$_page .= 'PEMBERIAN '.strtoupper($jenis_bantuan_sk).'<br /><br />';
$_page .= 'TAHUN ANGGARAN '.$tahun_anggaran;
$_page .= '<br /><br />ANTARA<br /><br />';
$_page .= 'PEJABAT PEMBUAT KOMITMEN BIRO KEUANGAN <br />';
$_page .= 'SEKRETARIAT JENDERAL KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN';
$_page .= '<br /><br />DENGAN <br /><br />'.$instansi->title;
$_page .= '
</td>
</tr>
</table>&nbsp;<br />
<table>
<tr>
    <td width="28%">&nbsp;</td>
    <td width="15%">NOMOR</td>
    <td width="5%"> : </td>
';
$_page .= '  <td>'.$sk->title.'/A2.1/KU/2018</td>';
$_page .= '
</tr>
<tr>
  <td>&nbsp;</td>
  <td>TANGGAL</td>
    <td> : </td>
  ';
$_page .= '  <td>'.$tanggal_mou.'</td>';
$_page .= '
</tr>
</table>
&nbsp;<br />
<table>
<tr>
<td align="justify">
';
$_page .= 'Pada hari ini '.day2text($hari).' tanggal '.date2text($tanggal);
$_page .= ' bulan '.month2text($bulan).' tahun '.terbilang($tahun);
$_page .= '
telah diadakan Perjanjian Kerjasama Pemberian Bantuan Pemerintah pada Biro Keuangan Sekretariat Jenderal 
Kementerian Pendidikan dan Kebudayaan, antara :<br />
</td>
</tr>
</table>
<br />
<table>
  <tr>
    <td valign="top" width="3%">1.</td>
    <td width="97%">
      <table>
        <tr>
          <td width="20%">Nama </td>
          <td width="5%"> : </td>
          <td width="75%">Taopiq, S.Pd., M.M</td>
        </tr>
        <tr>
          <td>NIP </td>
          <td> : </td>
          <td>197703112000031002</td>
        </tr>
        <tr>
          <td valign="top">Jabatan </td>
          <td valign="top"> : </td>
          <td>Kepala Sub Bagian Pembiayaan, Biro Keuangan Sekretariat Jenderal</td>
        </tr>
        <tr>
          <td>Alamat </td>
          <td> : </td>
          <td>Jl. Jenderal Sudirman Senayan  Jakarta</td>
        </tr>
        <tr>
          <td align="justify" colspan="3">Bertindak untuk dan atas nama Biro Keuangan Sekretariat Jenderal Kementerian Pendidikan dan Kebudayaan selaku Pejabat Pembuat Komitmen (PPK), yang disebut PIHAK PERTAMA.<br /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top">2.</td>
    <td>
      <table>
        <tr>
          <td width="20%">Nama </td>
          <td width="5%"> : </td>
';
$_page .= '          <td width="75%">'.$instansi->field_lem_kontak['und'][0]['value'].'</td>';
$_page .= '
        </tr>
        <tr>
          <td>Pimpinan/Ketua</td>
          <td> : </td>
';
$_page .= '          <td>'.$instansi->title.'</td>';
$_page .= '
        </tr>
        <tr>
          <td valign="top">Alamat </td>
          <td valign="top"> : </td>
';
$_page .= '          <td align="justify">'.$alamat.'</td>';
$_page .= '
        </tr>
        <tr>
          <td align="justify" colspan="3">Bertindak untuk dan atas nama (nama lembaga/organisasi), yang selanjutnya disebut PIHAK KEDUA.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
&nbsp;<br />
';

$pdf->writeHTML($_page, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$_page = '
<table>
<tr>
<td align="center">
Pasal 1<br />
Ruang Lingkup Perjanjian Kerjasama<br />
</td>
</tr>
</table>
<table>
<tr>
<td align="justify">
PIHAK PERTAMA mengadakan perjanjian dengan PIHAK KEDUA berupa pemberian bantuan pemerintah pada Biro Keuangan Setjen Kemendikbud. 
</td>
</tr>
</table>
<!-- ------------------------------------------------ -->
<table>
<tr>
<td align="center">
&nbsp;<br />Pasal 2<br />
Tanggung Jawab<br />
</td>
</tr>
</table>
<table>
<tr>
  <td width="5%">
(1)
  </td>
  <td width="95%" align="justify">
PIHAK KEDUA bertanggungjawab terhadap pelaksanaan penggunaan dana bantuan yang diterima dari PIHAK PERTAMA;
  </td>
</tr>
<tr>
  <td>
(2)
  </td>
  <td align="justify">
Apabila terjadi penyalahgunaan terhadap penggunaan dana bantuan yang diterima dari PIHAK PERTAMA maka PIHAK KEDUA bertanggungjawab terhadap konsekuensi hukum yang berlaku.
  </td>
</tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
Pasal 3<br />
Hak dan Kewajiban PIHAK PERTAMA<br />
</td>
</tr>
</table>
<table>
<tr>
<td width="5%">(1)</td>
<td width="95%" align="justify">
PIHAK PERTAMA berhak:<br />
  <table>
    <tr>
      <td width="5%">a.</td>
      <td width="95%">Menetapkan lembaga/organisasi penerima bantuan;</td>
    </tr>
    <tr>
      <td>b.</td>
      <td>Menetapkan jumlah dana bantuan;</td>
    </tr>
    <tr>
      <td>c.</td>
      <td align="justify">Menerima laporan pertanggungjawaban penggunaan dana dan pelaksanaan bantuan dari PIHAK KEDUA sesuai ketentuan.</td>
    </tr>
  </table>
</td>
</tr>
<tr>
<td>(2)</td>
<td>
PIHAK PERTAMA berkewajiban:<br />
  <table>
    <tr>
      <td width="5%">a.</td>
      <td width="95%" align="justify">Melakukan pengecekan kelengkapan data dan verifikasi terhadap kelengkapan persyaratan proposal permohonan;</td>
    </tr>
    <tr>
      <td>b.</td>
      <td align="justify">Menyalurkan dana bantuan kepada PIHAK KEDUA melalui Bank penyalur sesuai dengan ketentuan;</td>
    </tr>
    <tr>
      <td>c.</td>
      <td align="justify">Melakukan monitoring dan evaluasi pelaksanaan bantuan oleh PIHAK KEDUA pada kondisi tertentu;</td>
    </tr>
    <tr>
      <td>d.</td>
      <td align="justify">Meminta laporan pelaksanaan pekerjaan yang dilakukan oleh PIHAK KEDUA;</td>
    </tr>
    <tr>
      <td>e.</td>
      <td align="justify">Memberikan teguran dan atau sanksi kepada PIHAK KEDUA, baik secara lisan maupun tertulis, apabila dalam pelaksanaan kegiatan dan penggunaan dana bantuan tersebut tidak sesuai dengan surat perjanjian kerjasama.</td>
    </tr>
  </table>

</td>
</tr>
</table>
<!-- ------------------------------------------------ -->
';
$pdf->writeHTML($_page, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$_page = '
<table>
<tr>
<td align="center">
Pasal 4<br />
Hak dan Kewajiban PIHAK KEDUA<br />
</td>
</tr>
</table>
<table>
<tr>
  <td width="5%">
    (1) 
  </td>
  <td width="95%" align="justify">
  PIHAK KEDUA berhak:<br />
  <table>
    <tr>
      <td width="5%">a.</td>
      <td width="95%" align="justify">Menerima dana bantuan dari PIHAK PERTAMA sesuai dengan surat perjanjian kerjasama;</td>
    </tr>
    <tr>
      <td>b.</td>
      <td align="justify">Menggunakan dana bantuan sesuai dengan petunjuk teknis penyaluran bantuan pemerintah dan RAB yang disepakati;</td>
    </tr>
  </table>
  </td>
</tr>  
<tr>
  <td width="5%">
    (2)
  </td>
  <td width="95%" align="justify">
    PIHAK KEDUA berkewajiban:<br />
    <table>
      <tr>
        <td width="5%">a.</td>
        <td width="95%" align="justify">Melaksanakan pekerjaan sesuai dengan yang telah disepakati dalam Perjanjian Kerja Sama antara PIHAK PERTAMA dan PIHAK KEDUA;</td>
      </tr>
      <tr>
        <td>b.</td>
        <td align="justify">Mempertanggungjawabkan penggunaan dana bantuan yang telah diterima sesuai dengan ketentuan dan peraturan yang berlaku dan tidak memberikan imbalan dalam bentuk apapun kepada siapapun yang terkait dengan penerimaan dana bantuan;</td>
      </tr>
      <tr>
        <td>c.</td>
        <td align="justify">Menyusun dan menyampaikan laporan pertanggungjawaban penggunaan dana dan pelaksanaan pekerjaan bantuan kepada PIHAK PERTAMA;</td>
      </tr>
      <tr>
        <td>d.</td>
        <td align="justify">Bertanggungjawab sepenuhnya terhadap segala bentuk penyimpangan, penyalahgunaan, dan pelanggaran penggunaan dana sesuai dengan peraturan dan perundang-undangan yang berlaku;</td>
      </tr>
      <tr>
        <td>e.</td>
        <td align="justify">Mentaati teguran/peringatan/sanksi yang disampaikan oleh PIHAK PERTAMA, baik secara lisan maupun tertulis.</td>
      </tr>
    </table>
  </td>
</tr>  
</table>
<!-- ------------------------------------------------ -->
<table>
<tr>
<td align="center">
&nbsp;<br />Pasal 5<br />
Jenis Pekerjaan<br />
</td>
</tr>
</table>
<table>
<tr>
';


if ($sk->field_sk_jenis_bantuan['und'][0]['tid'] == 1105) {

$_page .= '
<td align="justify">
PIHAK KEDUA menerima dana bantuan pemerintah dari PIHAK PERTAMA untuk melaksanakan pekerjaan sesuai dengan proposal yang diajukan berupa '.$jenis_rehab.'.
</td>
';

} else {
$_page .= '
<td align="justify">
PIHAK KEDUA menerima dana bantuan pemerintah dari PIHAK PERTAMA untuk melaksanakan pekerjaan sesuai dengan proposal yang diajukan berupa '.$jenis_bantuan_sk.'.
</td>
';

}


$_page .= '
</tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
Pasal 6<br />
Nilai dan Rincian Dana Bantuan<br />
</td>
</tr>
</table>
<table>
<tr>
  <td width="5%">(1)</td>
  <td width="95%" align="justify">Nilai dana bantuan yang diberikan PIHAK PERTAMA kepada PIHAK KEDUA sebesar Rp.
';
$_page .= 
  number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, '.', '.');
$_page .= '  ,- terbilang (';
$_page .= terbilang($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']).' rupiah';
$_page .= '
).
  </td>
</tr>
<tr>
  <td width="5%">(2)</td>
  <td width="95%" align="justify">Nilai bantuan sebagaimana dimaksud pada ayat (1) digunakan untuk '.$jenis_bantuan_sk.'.
  </td>
</tr>

</tr>
</table>
<!-- ------------------------------------------------ -->
';

$pdf->writeHTML($_page, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$_page = '
<table>
<tr>
<td align="center">
Pasal 7<br />
Jangka Waktu Penyelesaian Pekerjaan<br />
</td>
</tr>
</table>
<table>
<tr>
  <td width="5%">(1)</td>
  <td width="95%" align="justify">
Jangka waktu penyelesaian pekerjaan selama 30 (tiga puluh) hari kalender terhitung sejak dana diterima;
  </td>
</tr>
<tr>
  <td width="5%">(2)</td>
  <td width="95%" align="justify">
Jangka waktu pelaksanaan pekerjaan dapat diperpanjang atas persetujuan PIHAK PERTAMA, didasarkan pada surat permohonan perpanjangan dari PIHAK KEDUA dengan alasan yang dapat dipertanggungjawabkan.
  </td>
</tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
<br />Pasal 8<br />
Penyaluran Dana Bantuan<br />
</td>
</tr>
</table>
<table>
<tr>
  <td width="5%">(1)</td>
  <td width="95%" align="justify">
Penyaluran dana bantuan akan dilakukan setelah semua persyaratan dipenuhi dan surat perjanjian kerjasama ditandatangani oleh PIHAK PERTAMA dan PIHAK KEDUA; </td>
</tr>
<tr>
  <td width="5%">(2)</td>
  <td width="95%" align="justify">
Penyaluran dana bantuan pada ayat (1), dilakukan melalui:<br />
  <table>
    <tr>
      <td width="5%">a.</td>
      <td width="95%">Bendahara Pengeluaran; atau</td>
    </tr>
    <tr>
      <td width="5%">b.</td>
      <td width="95%">Proses pemindahbukuan secara langsung melalui Bank Negara Indonesia (BNI) KLN Kemendikbud Jakarta ke rekening PIHAK KEDUA:<br />
        <table>
          <tr>
            <td width="25%">Nama Bank</td>
            <td width="5%">:</td>
            <td width="70%">'.$nama_bank.'</td>
          </tr>
          <tr>
            <td>Cabang / Unit</td>
            <td>:</td>
            <td>'.$cabang_bank.'</td>
          </tr>
          <tr>
            <td>Nomor Rekening</td>
            <td>:</td>
            <td>'.$no_rekening.'</td>
          </tr>
          <tr>
            <td>Atas Nama</td>
            <td>:</td>
            <td>'.$nama_rekening.'</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</td>
</tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
<br />Pasal 9<br />
Sanksi<br />
</td>
</tr>
</table>
<table>
  <tr>
    <td width="5%">(1)</td>
    <td width="95%" align="justify">
Apabila berdasarkan hasil monitoring dan evaluasi yang dilakukan oleh PIHAK PERTAMA atau temuan aparat pengawas, ternyata PIHAK KEDUA terbukti melakukan kekeliruan/kesalahan dalam melaksanakan kegiatan/program yang telah disepakati, maka PIHAK PERTAMA akan menyampaikan teguran baik secara lisan maupun tertulis kepada PIHAK KEDUA;
    </td>
  </tr>
  <tr>
    <td width="5%">(2)</td>
    <td width="95%" align="justify">
Teguran PIHAK PERTAMA kepada PIHAK KEDUA berisi permintaan untuk memperbaiki/menyelesaikan segala bentuk kesalahan/kekeliruan yang telah dilakukan;
    </td>
  </tr>
  <tr>
    <td width="5%">(3)</td>
    <td width="95%" align="justify">
Apabila PIHAK KEDUA terbukti menggunakan dana tidak sesuai sebagaimana yang diatur dalam perjanjian kerja sama ini dan/atau digunakan untuk kepentingan pribadi, maka penerima bantuan wajib mengembalikan dana bantuan yang telah diterima ke Kas Negara;
    </td>
  </tr>
  <tr>
    <td width="5%">(4)</td>
    <td width="95%" align="justify">
Pengembalian bantuan sebagaimana dimaksud pada ayat (3) dilakukan PIHAK KEDUA melalui Bank BNI cabang setempat dengan:<br />
      <table>
        <tr>
          <td width="5%">a.</td>
          <td width="95%">Mengisi SSPB (Surat Setoran Pengembalian Belanja) apabila dalam tahun anggaran berjalan dengan kode MAP (disesuaikan dengan kode akun pengeluaran);</td>
        </tr>
        <tr>
          <td width="5%">b.</td>
          <td width="95%">Mengisi SSBP (Surat Setoran Bukan Pajak) apabila tahun anggaran berikutnya dengan kode MAP 423958.</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="5%">(5)</td>
    <td width="95%" align="justify">
Apabila PIHAK KEDUA tidak mampu mengembalikan dana bantuan sebagaimana dimaksud pada ayat (3) di atas, maka PIHAK KEDUA tidak akan diberikan bantuan lagi oleh PIHAK PERTAMA pada tahun-tahun berikutnya.
    </td>
  </tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
<br />Pasal 10<br />
Pelaporan dan Pertanggungjawaban<br />
</td>
</tr>
</table>
<table>
  <tr>
    <td width="5%">(1)</td>
    <td width="95%" align="justify">
PIHAK KEDUA wajib menyusun dan menyampaikan laporan pertanggungjawaban kepada PIHAK PERTAMA paling lambat 30 (tiga puluh) hari setelah pekerjaan selesai;
    </td>
  </tr>
  <tr>
    <td width="5%">(2)</td>
    <td width="95%" align="justify">
Laporan pertanggungjawaban sesuai yang disebutkan pada ayat (1) tersebut harus dilampiri:<br />
      <table>
        <tr>
          <td width="5%">a.</td>
          <td width="95%">
Laporan Pertanggungjawaban Bantuan Operasional (khusus untuk bantuan operasional);
          </td>
        </tr>
        <tr>
          <td width="5%">b.</td>
          <td width="95%">
Berita Acara Serah Terima (khusus untuk bantuan sarana/prasarana,  bantuan rehabilitasi/pembangunan gedung/bangunan, dan bantuan lainnya yang ditetapkan oleh PA);
          </td>
        </tr>
        <tr>
          <td width="5%">c.</td>
          <td width="95%">
Bukti surat setoran sisa dana (apabila ada);
          </td>
        </tr>
        <tr>
          <td width="5%">d.</td>
          <td width="95%">
Dokumentasi/foto kegiatan atau barang yang dihasilkan/dibeli;
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="5%">(3)</td>
    <td width="95%" align="justify">
Bukti-bukti yang sah (kuitansi pengeluaran bermaterai, pembelian material, dan bukti penyetoran pajak (bila ada), serta bukti-bukti lainnya disimpan oleh PIHAK KEDUA sebagai dokumen pemeriksaan.
    </td>
  </tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
Pasal 11<br />
Penanggungan Resiko<br />
</td>
</tr>
</table>
<table>
<tr>
<td align="justify">
PIHAK KEDUA berkewajiban untuk membebaskan dan menanggung tanpa batas PIHAK PERTAMA beserta instansinya terhadap akibat yang timbul atas semua konsekuensi hukum dan biaya sehubungan dengan ditandatanganinya perjanjian ini.
</td>
</tr>
</table>
<!-- ------------------------------------------------ -->
';

$pdf->writeHTML($_page, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$_page = '

&nbsp;<br />
<table>
<tr>
<td align="center">
<br />Pasal 12<br />
Keadaan Memaksa (<i>Force Majeure</i>)<br />
</td>
</tr>
</table>
<table>
  <tr>
    <td width="5%">(1)</td>
    <td width="95%" align="justify">
Yang dimaksud keadaan memaksa (<i>Force Majeure</i>) adalah peristiwa seperti: bencana alam (gempa bumi, tanah longsor, banjir), kebakaran, perang, huru-hara, pemogokkan, pemberontakan, dan epidemi yang secara keseluruhan ada hubungan langsung dengan penyelesaian pekerjaan;
    </td>
  </tr>
  <tr>
    <td width="5%">(2)</td>
    <td width="95%" align="justify">
Apabila terjadi keadaan <i>Force Majeure</i> sebagaimana dimaksud pada ayat (1) di atas, maka kedua belah pihak setuju untuk merevisi surat perjanjian dan pelaksanaan pekerjaan.
    </td>
  </tr>
</table>
<!-- ------------------------------------------------ -->
&nbsp;<br />
<table>
<tr>
<td align="center">
<br />Pasal 13<br />
Lain-lain<br />
</td>
</tr>
</table>
<table>
  <tr>
    <td width="5%">(1)</td>
    <td width="95%" align="justify">
Surat Perjanjian ini dianggap sah setelah ditandatangani oleh kedua belah pihak;
    </td>
  </tr>
  <tr>
    <td width="5%">(2)</td>
    <td width="95%" align="justify">
Biaya materai dalam Surat Perjanjian Kerjasama ini dibebankan kepada PIHAK KEDUA;
    </td>
  </tr>
  <tr>
    <td width="5%">(3)</td>
    <td width="95%" align="justify">
Perubahan atas Surat Perjanjian Kerjasama ini dapat dilakukan atas persetujuan kedua belah pihak;
    </td>
  </tr>
  <tr>
    <td width="5%">(4)</td>
    <td width="95%" align="justify">
Surat Perjanjian ini dibuat rangkap 2 (dua), lembar pertama dan kedua masing-masing dibubuhi materai Rp6.000,- (enam ribu rupiah);
    </td>
  </tr>
  <tr>
    <td width="5%">(5)</td>
    <td width="95%" align="justify">
Dokumen ini beserta lampirannya merupakan bagian yang tidak terpisahkan dari surat perjanjian kerjasama.
    </td>
  </tr>
</table>
&nbsp;<br />
<table>
<tr>
<td align="right" width="90%">Jakarta, '.$tanggal_mou.'</td><td width="10%"></td>
</tr>
</table>
&nbsp;<br />
<table>
<tr>
<td width="50%" align="center">PIHAK PERTAMA<br />
Pejabat Pembuat Komitmen,<br />
Bagian Perbendaharaan dan Pembiayaan<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
Taopiq, S.Pd, M.M<br />
NIP. 197703112000031002
</td>
<td width="50%" align="center">
PIHAK KEDUA<br />
Pimpinan / Ketua<br />
&nbsp;<br />
&nbsp;<br />
<span style="font-size: 8px;">materai Rp6.000,-dan stempel</span>&nbsp;
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
'.$penanggungjawab.'
</td>
</tr>
</table>

&nbsp;<br />
&nbsp;<br />
<table>
<tr>
<td style="font-size: smaller;">
*) Dibuat rangkap 2 (dua),<br />
1 (satu) untuk disimpan oleh PIHAK PERTAMA (bermaterai),<br />
1 (satu) untuk disimpan oleh PIHAK KEDUA (bermaterai)</td>
</tr>
</table>
';

#echo $_page;
$pdf->writeHTML($_page, true, false, true, false, '');

$pdf->AddPage('L', 'A4');
$pdf->setCellHeightRatio(1.1);

$_page = '
<table>
  <tr>
    <td align="center">
<b>KWITANSI<br />KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</b>
    </td>
  </tr>
</table>
&nbsp;<br />&nbsp;<br />
<table>
  <tr>
    <td width="20%" height="30px">
Nomor Kwitansi
    </td>
    <td width="5%">
:
    </td>
    <td width="75%">
. . . . . . . . . . . . .
    </td>
  </tr>
  <tr>
    <td height="30px">
Tahun Anggaran
    </td>
    <td>
:
    </td>
    <td>
'.$tahun_anggaran.'
    </td>
  </tr>
  <tr>
    <td height="30px">
Kode Akun
    </td>
    <td>
:
    </td>
    <td>
. . . . . . . . . . . . .
    </td>
  </tr>
  <tr>
    <td height="30px">
Sudah Terima dari
    </td>
    <td>
:
    </td>
    <td>
Kementerian Pendidikan dan Kebudayaan
    </td>
  </tr>
  <tr>
    <td height="30px">
Banyaknya Uang
    </td>
    <td>
:
    </td>
    <td>
'.number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, '.', '.').'
('.terbilang($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']).' rupiah)
    </td>
  </tr>
  <tr>
    <td height="30px">
Untuk Pembayaran
    </td>
    <td>
:
    </td>
    <td>
Pemberian '.$jenis_bantuan_sk.' kepada perseorangan, satuan pendidikan, komunitas budaya, dan
lembaga/organisasi masyarakat lainnya yang bergerak dibidang pendidikan dan kebudayaan Tahun '.$tahun_anggaran.'
    </td>
  </tr>
</table>
&nbsp;<br />&nbsp;<br />
<table>
  <tr>
    <td width="35%">
Setuju dibayar,<br />
      Pejabat Pembuat Komitmen<br />
      Bagian Perbendaharaan dan Pembiayaan,<br />
      Biro Keuangan.
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
Taopiq, S.Pd., MM<br />
NIP.197703112000031002
    </td>
    <td width="35%">
Lunas dan telah dibukukan
      tgl. '.$tanggal_mou.'<br />
      Bendahara Pengeluaran Pembantu Bantuan Pemerintah
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
Dwi Prabowo, S.Kom<br />
NIP.198408082009121005</td>
    <td width="30%">
Jakarta, '.$tanggal_mou.'<br />
      Penerima<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
<span style="font-size: 8px;">materai Rp6.000,-dan stempel</span>&nbsp;
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />

      '.$penanggungjawab.'
    </td>
  </tr>

</table>
';

$pdf->writeHTML($_page, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

$pdf->Output('mou-sarpas.pdf', 'I');

} else {
  die('data tidak tersedia');
}










  #die('oke disini dulu');
} else {
  die('Error parameter!');
}
?>
