<?php

#define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
#define ('DRUPAL_ROOT', '/var/www/html/projects/openrecords/or2');
define ('DRUPAL_ROOT', '/var/www/html/openrecords/or2');
require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$nid = arg(1);

if (!is_numeric($nid)) {
  die('Error: Bad URL defined.');
}

$surtug = node_load($nid);

if (empty($surtug)) {
  die('Error: Not a node.');
}

if ($surtug->type != 'surat_tugas') {
  die('Error: Invalid type.');
}

#var_dump($surtug->field_srttugas_personil); die();
#echo count($surtug->field_srttugas_personil['und']); die();
$_jumlah_orang = count($surtug->field_srttugas_personil['und']);

#$is_dirjen = FALSE;
#$is_direktorat = FALSE;
#$is_lpmp = FALSE;
/**
if ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 24) {
  $is_dirjen = TRUE;
} elseif ( ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 13) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 18) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 28) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 34) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 40) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 46) ) {
  $is_direktorat = TRUE;
} elseif ( ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 52)  ) {

}
**/
#var_dump($surtug->field_srt_unit_kerja);
$_unitkerja = taxonomy_term_load($surtug->field_srt_unit_kerja['und'][0]['target_id']);
#var_dump($_unitkerja->field_uk_status['und'][0]['value']);
#var_dump($_unitkerja->name);
#die();

function month2text($month) {
  if ($month == '01') {
    return 'Januari';
  } elseif ($month == '02') {
    return 'Februari';
  } elseif ($month == '03') {
    return 'Maret';
  } elseif ($month == '04') {
    return 'April';
  } elseif ($month == '05') {
    return 'Mei';
  } elseif ($month == '06') {
    return 'Juni';
  } elseif ($month == '07') {
    return 'Juli';
  } elseif ($month == '08') {
    return 'Agustus';
  } elseif ($month == '09') {
    return 'September';
  } elseif ($month == '10') {
    return 'Oktober';
  } elseif ($month == '11') {
    return 'November';
  } elseif ($month == '12') {
    return 'Desember';
  }
}


  $__tanggal = explode(" ", $surtug->field_srt_tanggal['und'][0]['value']);
  $_tanggal = explode("-", $__tanggal[0]);
  #var_dump($_tanggal); die();
  $tanggal = $_tanggal[2].' '.month2text($_tanggal[1]).' '.$_tanggal[0];
  #$tanggal = $_tanggal[2].' '.$_tanggal[1].' '.$_tanggal[0];
  #echo ($tanggal); die();

#echo $tanggal;die();
#var_dump($surtug->field_srt_tanggal_kegiatan); die();

#$_unitkerja = taxonomy_term_load($surtug->field_srt_unit_kerja['und'][0]['target_id']);
#var_dump($_unitkerja->field_uk_status['und'][0]['value']);
#die();


 require_once '../tcpdf.php';

 class DikdasmenPDF extends TCPDF {
 public function Header() {
   global $_unitkerja;
        $image_file = "<img src=\"images/kemdikbud_v2-bw.png\" width=\"200\" />";
  $this->SetMargins('10', PDF_MARGIN_TOP, '10');
  $this->SetY(5);
  $this->SetFont('times', '', 12);
  $isi_header="<table align=\"center\">
     <tr>
      <td width=\"16%\">".$image_file."</td>
      <td width=\"84%\" height=\"114\" align=\"center\" style=\"line-height: 120%;\">
      <span style=\"font-size:16px;\">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</span><br />
";

if ($_unitkerja->field_uk_status['und'][0]['value'] == 1) {
$isi_header .= "     <span style=\"font-size:14px; font-weight: bold;\">DIREKTORAT JENDERAL</span><br />
      <span style=\"font-size:14px; font-weight: bold;\">PENDIDIKAN DASAR DAN MENENGAH</span><br />
";
} elseif ($_unitkerja->field_uk_status['und'][0]['value'] == 2) {
  if ($_unitkerja->tid == 46) {
    $isi_header .= "     <span style=\"font-size:14px;\">DIREKTORAT JENDERAL PENDIDIKAN DASAR DAN MENENGAH</span><br />
      <span style=\"font-size:14px; font-weight: bold;\">DIREKTORAT PEMBINAAN<br />PENDIDIKAN KHUSUS DAN LAYANAN KHUSUS</span><br />";
  
  } else {
    $isi_header .= "     <span style=\"font-size:14px;\">DIREKTORAT JENDERAL PENDIDIKAN DASAR DAN MENENGAH</span><br />
      <span style=\"font-size:14px; font-weight: bold;\"> ".strtoupper($_unitkerja->name)."</span><br />";
    }


} elseif ($_unitkerja->field_uk_status['und'][0]['value'] == 3) {
$isi_header .= "     <span style=\"font-size:14px;\">LEMBAGA PENJAMINAN MUTU PENDIDIKAN</span><br />
      <span style=\"font-size:14px; font-weight: bold;\">PROVINSI ".strtoupper(preg_replace("/LPMP\ /i", "", $_unitkerja->name))."</span><br />
";

}

$isi_header .= "       <span style=\"font-size:12px; line-height: 1.0;\">Jalan Jenderal Sudirman Gedung E Lantai 5 Komplek Kemdikbud Senayan, Jakarta 10270</span><br />
      <span style=\"font-size:12px; line-height: 1.0;\">Telepon (021) 5725610 Faksimili (021) 5725610</span><br />
      <span style=\"font-size:12px;\">Laman: www.dikdasmen.kemdikbud.go.id</span>
      </td>
     </tr>
    </table><hr>";
  $this->writeHTML($isi_header, true, false, false, false, '');
    }

/**
 public function Footer() {
        $image_file = "Danny Boy";
  $this->SetY(-40);
  $this->writeHTML($image_file, true, false, false, false, '');
        $this->SetY(-15);
  $this->writeHTML("<hr>", true, false, false, false, '');
        $this->SetFont('helvetica', '', 12);
        $this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
**/
}

 $pdf = new DikdasmenPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
   
 $pdf->SetTitle('Surat Tugas Dikdasmen');
 $pdf->SetSubject('Surat Tugas Dikdasmen');
   
 #$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
 $pdf->SetMargins('23', PDF_MARGIN_TOP, '20');
 $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
 #$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
   
 $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 $pdf->AddPage();
   
 $pdf->SetFont('times', '', 12);
 $pdf->SetY(50);
 $isi = "
<table>
  <tr>
    <td align=\"centre\">SURAT TUGAS</td>
  </tr>
  <tr>
    <td align=\"centre\">Nomor ............................</td>
  </tr>
  <tr>
    <td align=\"justify\">&nbsp;<br />
      Sekretaris Direktorat Jenderal Pendidikan Dasar dan Menengah memberikan tugas kepada, <br />
    </td>
  </tr>
</table>
";

if ($_jumlah_orang == 1) {
$isi .= "
<table>
  <tr>
    <td align=\"justify\">
      jika satu
    </td>
  </tr>
</table>
";
} elseif($_jumlah_orang > 1) {
  $isi .= "
<table border=\"1\">
      <tr>
        <td align=\"centre\" width=\"5%\">
           No
        </td>
        <td align=\"centre\" width=\"30%\">
          Nama 
        </td>
        <td align=\"centre\" width=\"20%\">
          NIP 
        </td>
        <td align=\"centre\" width=\"30%\">
          Pangkat dan golongan 
        </td>
        <td align=\"centre\" width=\"15%\">
          Jabatan 
        </td>
      </tr>
";
  foreach ($surtug->field_srttugas_personil['und'] as $_k => $_v) {
    #var_dump($_v['target_id']); die();
    $_personil = node_load($_v['target_id']);
    #var_dump($_personil->title);
    #die();
    $isi .= "
      <tr>
        <td>
          ".$_k." 
        </td>
        <td>
          ".$_personil->title." 
        </td>
        <td>
          ".$_personil->field_personal_nip['und'][0]['value']." 
        </td>
        <td>
          ";
          $_pangkat_golongan = taxonomy_term_load ($_personil->field_personal_pangkat_golongan['und'][0]['target_id']);
          $isi .= $_pangkat_golongan->name;
    $isi .= " 
        </td>
        <td>
          ".$_personil->field_personal_jabatan['und'][0]['value']." 
        </td>
      </tr>
    ";
  }

$isi .= "
</table>
";
}

$isi .= "
</table>
 <br /><table>
     <tr>
      <td width=\"70%\">
        <table align=\"left\">
          <tr>
            <td width=\"15%\">Nomor</td>
            <td width=\"5%\"> : </td>
            <td width=\"80%\">".$surtug->field_srt_nomor_surat['und'][0]['value']."</td>
          </tr>
          <tr>
            <td>Hal</td>
            <td> : </td>
            <td>".$surtug->title."</td>
          </tr>";

if (isset($surtug->field_srt_lampiran['und'][0]['value'])) {
$isi .=   "       <tr>
            <td>Lampiran</td>
            <td> : </td>
            <td align=\"left\">".$surtug->field_srt_lampiran['und'][0]['value']."</td>
          </tr> ";
}

$isi .=     "   </table>
      </td>
      <td width=\"30%\"><span style=\"font-size:12px;\">".$tanggal."</span></td>
     </tr>
    </table>

&nbsp;<br>&nbsp;<br><table>
  <tr>
    <td>".$surtug->field_srt_kepada['und'][0]['value']."</td>
  </tr>
</table>
&nbsp;<br>&nbsp;<br><table>
  <tr>
    <td align=\"justify\">".$surtug->field_srt_isi_surat['und'][0]['value']."</td>
  </tr>
</table>


";

if ($surtug->field_srt_info_kegiatan['und'][0]['value']) {
####################
  $isi .= "
&nbsp;<br><table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:1100px\">
  <tbody>
    <tr>
      <td valign=\"top\" style=\"width:60px\">&nbsp;Waktu</td>
      <td valign=\"top\" style=\"width:10px\">:</td>
      <td>";

  $__tanggal_awal = explode(" ", $surtug->field_srt_tanggal_kegiatan['und'][0]['value']);
  $_tanggal_awal = explode("-", $__tanggal_awal[0]);
  #var_dump($_tanggal); die();
  $tanggal_awal = $_tanggal_awal[2].' '.month2text($_tanggal_awal[1]).' '.$_tanggal_awal[0];
  $_jam_awal = explode(":", $__tanggal_awal[1]);
  $jam_awal = $_jam_awal[0]+7;
  $jam_awal = $jam_awal.':'.$_jam_awal[1];
  #$tanggal = $_tanggal[2].' '.$_tanggal[1].' '.$_tanggal[0];
  if (isset($surtug->field_srt_tanggal_kegiatan['und'][0]['value2'])) {
    $__tanggal_akhir = explode(" ", $surtug->field_srt_tanggal_kegiatan['und'][0]['value2']);
    $_tanggal_akhir = explode("-", $__tanggal_akhir[0]);
    #var_dump($_tanggal); die();
    $tanggal_akhir = $_tanggal_akhir[2].' '.month2text($_tanggal_akhir[1]).' '.$_tanggal_akhir[0];
    $_jam_akhir = explode(":", $__tanggal_akhir[1]);
    $jam_akhir = $_jam_akhir[0]+7;
    $jam_akhir = $jam_akhir.':'.$_jam_akhir[1];
  }


  $isi .= $tanggal_awal.' pukul '.$jam_awal;
  if (isset($surtug->field_srt_tanggal_kegiatan['und'][0]['value2'])) {
    $isi .= ' - '.$tanggal_akhir.' pukul '.$jam_akhir;
  }

   $isi .= "</td>
    </tr>
    <tr>
      <td valign=\"top\" style=\"width:60px\">&nbsp;Tempat</td>
      <td valign=\"top\" style=\"width:10px\">:</td>
      <td>".$surtug->field_srt_tempat_kegiatan['und'][0]['value']."</td>
    </tr>
  </tbody>
</table>
  ";
####################
if (isset($surtug->field_srt_penutup_surat['und'][0])) {
	$isi .= "&nbsp;<br><table>";
	$isi .= "<tr>";
	$isi .= "<td align=\"justify\">";
	$isi .= $surtug->field_srt_penutup_surat['und'][0]['value'];
	$isi .= "</td>";
	$isi .= "</tr>";
	$isi .= "</table>";
}
}


$isi .= "
<p>&nbsp;<br />
      <table align=\"left\">
          <tr>
            <td width=\"50%\">&nbsp;</td>
            <td width=\"50%\">&nbsp;<br />";

if ($surtug->field_srt_apakah_atas_nama['und'][0]['value'] == 1) {
  $isi .= "            <span>a.n. ".$surtug->field_srt_nama_jabatan['und'][0]['value']."</span><br />";
} else {
  $isi .= "            <span>".$surtug->field_srt_nama_jabatan['und'][0]['value']."</span><br />";
}

if ($surtug->field_srt_apakah_atas_nama['und'][0]['value'] == 1) {
  $isi .= $surtug->field_srt_an_nama_jabatan['und'][0]['value'];
}

$isi .= "            </td>
          </tr>
          <tr>
            <td width=\"50%\">&nbsp;</td>
            <td width=\"50%\">&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />
            <span>".$surtug->field_srt_penandatangan['und'][0]['value']."</span><br />
            NIP ".$surtug->field_srt_nip['und'][0]['value']."
            </td>
          </tr>";

if (isset($surtug->field_srt_tembusan['und'][0])) {
$isi .= "          <tr>
            <td width=\"50%\">
            Tembusan:";

#$isi .= "&nbsp; Ditjen Dikdasmen";
#$isi .= "<ul>";
foreach ($surtug->field_srt_tembusan['und'] as $_k => $_v) {
	#$isi .= "<li>".$_v['value']."</li>";
	$isi .= "<br />&nbsp;&nbsp;&nbsp;- ".$_v['value'];
}

#$isi .= "</ul>";
$isi .= "
</td>
            <td width=\"50%\">&nbsp;
            </td>
          </tr>
";
}

$isi .= "        </table>
  
</p>

";

 $pdf->writeHTML($isi, true, false, false, false, '');
     
 $namaPDF = 'laporan.pdf';
 $pdf->Output($namaPDF,'I');
?>
