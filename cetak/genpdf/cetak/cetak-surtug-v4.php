<?php

#define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
#define ('DRUPAL_ROOT', '/var/www/html/projects/openrecords/or2');
#define ('DRUPAL_ROOT', '/var/www/html/openrecords/or5');
define ('DRUPAL_ROOT', '/var/www/html/latihan');
require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$nid = arg(1);

if (!is_numeric($nid)) {
  die('Error: Bad URL defined.');
}

$surtug = node_load($nid);

if (empty($surtug)) {
  die('Error: Not a node.');
}

if ($surtug->type != 'surat_tugas') {
  die('Error: Invalid type.');
}

/**
$_onepersonil = node_load($surtug->field_srttugas_personil['und'][0]['target_id']);
var_dump($_onepersonil->field_personal_nip['und'][0]['safe_value']);
echo '<hr />Nama: '.$_onepersonil->title;
echo '<hr />NIP: '.$_onepersonil->field_personal_nip['und'][0]['safe_value'];
$_term_pangkat = taxonomy_term_load($_onepersonil->field_personal_pangkat_golongan['und'][0]['target_id']);
echo '<hr />Pangkat dan golongan: '.$_term_pangkat->name;
echo '<hr />Jabatan: '.$_onepersonil->field_personal_jabatan['und'][0]['value'];
die();
**/
#var_dump($surtug->field_srttugas_kota['und'][0]['value']); die();

#echo count($surtug->field_srttugas_personil['und']); die();
$_jumlah_orang = count($surtug->field_srttugas_personil['und']);

#$is_dirjen = FALSE;
#$is_direktorat = FALSE;
#$is_lpmp = FALSE;
/**
if ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 24) {
  $is_dirjen = TRUE;
} elseif ( ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 13) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 18) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 28) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 34) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 40) OR ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 46) ) {
  $is_direktorat = TRUE;
} elseif ( ($surtug->field_srt_unit_kerja['und'][0]['target_id'] == 52)  ) {

}
**/
#var_dump($surtug->field_srt_unit_kerja);
#$_unitkerja = taxonomy_term_load($surtug->field_srt_unit_kerja['und'][0]['target_id']);
#var_dump($_unitkerja->field_uk_status['und'][0]['value']);
#var_dump($_unitkerja->name);
#die();
$_end = end($surtug->field_unit_kerja_v2['und']);
#var_dump($_end); die();
#$_unitkerja = taxonomy_term_load($surat->field_srt_unit_kerja['und'][0]['target_id']);
$_unitkerja = taxonomy_term_load($_end['tid']);

if (is_null($_unitkerja->field_uk_status['und'])) {
  #die('nasuk sini');
  $_c = count($surtug->field_unit_kerja_v2['und']);
  #var_dump($surat->field_unit_kerja_v2['und']); die();
  #var_dump($_c); die();
  $_unitkerja = taxonomy_term_load($surtug->field_unit_kerja_v2['und'][$_c - 2]['tid']);
}


function month2text($month) {
  if ($month == '01') {
    return 'Januari';
  } elseif ($month == '02') {
    return 'Februari';
  } elseif ($month == '03') {
    return 'Maret';
  } elseif ($month == '04') {
    return 'April';
  } elseif ($month == '05') {
    return 'Mei';
  } elseif ($month == '06') {
    return 'Juni';
  } elseif ($month == '07') {
    return 'Juli';
  } elseif ($month == '08') {
    return 'Agustus';
  } elseif ($month == '09') {
    return 'September';
  } elseif ($month == '10') {
    return 'Oktober';
  } elseif ($month == '11') {
    return 'November';
  } elseif ($month == '12') {
    return 'Desember';
  }
}


  $__tanggal = explode(" ", $surtug->field_srt_tanggal['und'][0]['value']);
  $_tanggal = explode("-", $__tanggal[0]);
  #var_dump($_tanggal); die();
  $tanggal = $_tanggal[2].' '.month2text($_tanggal[1]).' '.$_tanggal[0];
  #$tanggal = $_tanggal[2].' '.$_tanggal[1].' '.$_tanggal[0];
  #echo ($tanggal); die();

#echo $tanggal;die();
#var_dump($surtug->field_srt_tanggal_kegiatan); die();

#$_unitkerja = taxonomy_term_load($surtug->field_srt_unit_kerja['und'][0]['target_id']);
#var_dump($_unitkerja->field_uk_status['und'][0]['value']);
#die();


 require_once '../tcpdf.php';

 class DikdasmenPDF extends TCPDF {
 public function Header() {
   global $_unitkerja;
        $image_file = "<img src=\"images/kemdikbud_v2-bw.png\" width=\"200\" />";
  $this->SetMargins('10', PDF_MARGIN_TOP, '10');
  $this->SetY(5);
  $this->SetFont('times', '', 12);
  $isi_header="<table align=\"center\">
     <tr>
      <td width=\"16%\">".$image_file."</td>
      <td width=\"84%\" height=\"114\" align=\"center\" style=\"line-height: 120%;\">
      <span style=\"font-size:16px;\">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</span><br />
";

if ($_unitkerja->field_uk_status['und'][0]['value'] == 1) {
$isi_header .= "     <span style=\"font-size:14px; font-weight: bold;\">DIREKTORAT JENDERAL</span><br />
      <span style=\"font-size:14px; font-weight: bold;\">PENDIDIKAN DASAR DAN MENENGAH</span><br />
";
} elseif ($_unitkerja->field_uk_status['und'][0]['value'] == 2) {
  if ($_unitkerja->tid == 46) {
    $isi_header .= "     <span style=\"font-size:14px;\">DIREKTORAT JENDERAL PENDIDIKAN DASAR DAN MENENGAH</span><br />
      <span style=\"font-size:14px; font-weight: bold;\">DIREKTORAT PEMBINAAN<br />PENDIDIKAN KHUSUS DAN LAYANAN KHUSUS</span><br />";
  
  } else {
    $isi_header .= "     <span style=\"font-size:14px;\">DIREKTORAT JENDERAL PENDIDIKAN DASAR DAN MENENGAH</span><br />
      <span style=\"font-size:14px; font-weight: bold;\"> ".strtoupper($_unitkerja->name)."</span><br />";
    }


} elseif ($_unitkerja->field_uk_status['und'][0]['value'] == 3) {
$isi_header .= "     <span style=\"font-size:14px;\">LEMBAGA PENJAMINAN MUTU PENDIDIKAN</span><br />
      <span style=\"font-size:14px; font-weight: bold;\">PROVINSI ".strtoupper(preg_replace("/LPMP\ /i", "", $_unitkerja->name))."</span><br />
";

}

$isi_header .= "       <span style=\"font-size:12px; line-height: 1.0;\">Jalan Jenderal Sudirman Gedung E Lantai 5 Komplek Kemdikbud Senayan, Jakarta 10270</span><br />
      <span style=\"font-size:12px; line-height: 1.0;\">Telepon (021) 5725610 Faksimili (021) 5725610</span><br />
      <span style=\"font-size:12px;\">Laman: www.dikdasmen.kemdikbud.go.id</span>
      </td>
     </tr>
    </table><hr>";
  $this->writeHTML($isi_header, true, false, false, false, '');
    }

/**
 public function Footer() {
        $image_file = "Danny Boy";
  $this->SetY(-40);
  $this->writeHTML($image_file, true, false, false, false, '');
        $this->SetY(-15);
  $this->writeHTML("<hr>", true, false, false, false, '');
        $this->SetFont('helvetica', '', 12);
        $this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
**/
}

 $pdf = new DikdasmenPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
   
 $pdf->SetTitle('Surat Tugas Dikdasmen');
 $pdf->SetSubject('Surat Tugas Dikdasmen');
   
 #$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
 $pdf->SetMargins('23', PDF_MARGIN_TOP, '20');
 $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
 #$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
   
 $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 $pdf->AddPage();
   
 $pdf->SetFont('times', '', 12);
 $pdf->SetY(50);
 $isi = "
<table>
  <tr>
    <td align=\"centre\">SURAT TUGAS</td>
  </tr>
  <tr>
    <td align=\"centre\">Nomor ............................</td>
  </tr>
  <tr>
    <td align=\"justify\">&nbsp;<br />
      Sekretaris Direktorat Jenderal Pendidikan Dasar dan Menengah memberikan tugas kepada, <br />
    </td>
  </tr>
</table>
";

if ($_jumlah_orang == 1) {
  $_onepersonil = node_load($surtug->field_srttugas_personil['und'][0]['target_id']);
  #echo '<hr />Nama: '.$_onepersonil->title;
  #echo '<hr />NIP: '.$_onepersonil->field_personal_nip['und'][0]['safe_value'];
  $_term_pangkat = taxonomy_term_load($_onepersonil->field_personal_pangkat_golongan['und'][0]['target_id']);
  #echo '<hr />Pangkat dan golongan: '.$_term_pangkat->name;
  #echo '<hr />Jabatan: '.$_onepersonil->field_personal_jabatan['und'][0]['value'];
$isi .= "
<table border=\"0\">
  <tr>
    <td width=\"30%\" height=\"18px\">
      nama
    </td>
    <td width=\"5%\">
      :
    </td>
    <td width=\"65%\">
      ".$_onepersonil->title."
    </td>
  </tr>
  <tr>
    <td width=\"30%\" height=\"18px\">
      NIP
    </td>
    <td width=\"5%\">
      :
    </td>
    <td width=\"65%\">
      ".$_onepersonil->field_personal_nip['und'][0]['safe_value']."
    </td>
  </tr>
  <tr>
    <td width=\"30%\" height=\"18px\">
      pangkat dan golongan
    </td>
    <td width=\"5%\">
      :
    </td>
    <td width=\"65%\">
      ".$_term_pangkat->name."
    </td>
  </tr>
  <tr>
    <td width=\"30%\" height=\"18px\">
      jabatan
    </td>
    <td width=\"5%\">
      :
    </td>
    <td width=\"65%\">
      ".$_onepersonil->field_personal_jabatan['und'][0]['value']."
    </td>
  </tr>
</table>
";
} elseif($_jumlah_orang > 1) {
  $isi .= "
<table border=\"1\">
      <tr>
        <td width=\"6%\">
           No
        </td>
        <td align=\"centre\" width=\"59%\">
          Nama, NIP, Pangkat dan Golongan 
        </td>
        <td align=\"centre\" width=\"35%\">
          Jabatan 
        </td>
      </tr>
";
  foreach ($surtug->field_srttugas_personil['und'] as $_k => $_v) {
    #var_dump($_v['target_id']); die();
    $_personil = node_load($_v['target_id']);
    #var_dump($_personil->title);
    #die();
    $_no = $_k + 1;
    $isi .= "
      <tr>
        <td>
          &nbsp;<br />&nbsp;&nbsp;".$_no."&nbsp;<br /> 
        </td>
        <td>&nbsp;<br />&nbsp;
          ".$_personil->title." &nbsp;<br />
        </td>
        <td>&nbsp;<br />&nbsp;
          ".$_personil->field_personal_jabatan['und'][0]['value']." 
        </td>
      </tr>
    ";
  }

$isi .= "
</table>
";
}



$isi .=     "
&nbsp;<br><table>
  <tr>
    <td align=\"justify\">".$surtug->field_srt_isi_surat['und'][0]['value']."</td>
  </tr>
</table>
";

$isi .= "
<p>&nbsp;<br />
      <table align=\"left\">

          <tr>
            <td width=\"50%\">&nbsp;</td>
            <td width=\"50%\">".$surtug->field_srttugas_kota['und'][0]['value'].', '.$tanggal."
            </td>
          </tr>

          <tr>
            <td width=\"50%\">&nbsp;</td>
            <td width=\"50%\">&nbsp;<br />";

if ($surtug->field_srt_apakah_atas_nama['und'][0]['value'] == 1) {
  $isi .= "            <span>a.n. ".$surtug->field_srt_nama_jabatan['und'][0]['value']."</span><br />";
} else {
  $isi .= "            <span>".$surtug->field_srt_nama_jabatan['und'][0]['value']."</span><br />";
}

if ($surtug->field_srt_apakah_atas_nama['und'][0]['value'] == 1) {
  $isi .= $surtug->field_srt_an_nama_jabatan['und'][0]['value'];
}

$isi .= "            </td>
          </tr>
          <tr>
            <td width=\"50%\">&nbsp;</td>
            <td width=\"50%\">&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />
            <span>".$surtug->field_srt_penandatangan['und'][0]['value']."</span><br />
            NIP ".$surtug->field_srt_nip['und'][0]['value']."
            </td>
          </tr>";




if (isset($surtug->field_srt_tembusan['und'][0])) {
$isi .= "          <tr>
            <td width=\"50%\">
            Tembusan:";
#debug
#$isi .= '___'.count($surat->field_srt_tembusan['und']).'___';
#$isi .= "&nbsp; Ditjen Dikdasmen";
#$isi .= "<ul>";
foreach ($surtug->field_srt_tembusan['und'] as $_k => $_v) {
  #$isi .= "<li>".$_v['value']."</li>";
  $isi .= "<br />&nbsp;&nbsp;&nbsp;";
  if (count($surtug->field_srt_tembusan['und']) > 1) {
    $isi .= "- ";
  }
  $isi .= $_v['value'];
}
#$isi .= "</ul>";
$isi .= "
</td>
            <td width=\"50%\">&nbsp;
            </td>
          </tr>
";
}



$isi .= "        </table>
  
</p>

";

 $pdf->writeHTML($isi, true, false, false, false, '');
     
 $namaPDF = 'laporan.pdf';
 $pdf->Output($namaPDF,'I');
?>
