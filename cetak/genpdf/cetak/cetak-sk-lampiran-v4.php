<?php

$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];

$path = explode('/', $_path);

if (is_numeric($path[1])) {
  require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $_sk = node_load($path[1]);
  #var_dump($_sk); die();
  $_jenis_bantuan = taxonomy_term_load ($_sk->field_sk_jenis_bantuan['und'][0]['tid']);
  #$_total = number_format($_sk->field_sk_total['und'][0]['value']);
  $_total = number_format(get_totalNilai_perSk($path[1]), 0, '.', '.');

  if (!$_sk) {
    die('Invalid Node ID');
  }
  if ($_sk->type != 'sk') {
    die('Invalid Node Type');
  }

  #cek apakah status sudah menjadi SK
  if ($_sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Not ready for SK printing.');
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'proposal')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('field_sk', 'target_id', $path[1], '=')
    ->fieldOrderBy('field_pro_nomor_pendaftaran', 'value', 'DESC')
    #->range(0, 10)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));

  $result = $query->execute();
  if (isset($result['node'])) {

  $__tanggal = explode(" ", $_sk->field_sk_tanggal_dibuat['und'][0]['value']);
  $_tanggal = explode("-", $__tanggal[0]);
  #var_dump($_tanggal); die();
  $tanggal = $_tanggal[2].' '.month2text($_tanggal[1]).' '.$_tanggal[0];


###############################################

require_once('tcpdf_include.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 002');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
#$pdf->setPrintFooter(true);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
#$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
  require_once(dirname(__FILE__).'/lang/eng.php');
  $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 10);







    #echo count($result['node']); die();
    $total_rec = count($result['node']);

    $counter = 0;
    foreach ($result['node'] as $k => $v) {
      $counter++;

      if ( ($counter == 1) OR ($counter % 5 == 1) ) {

        // add a page
        #$pdf->AddPage();
        $pdf->AddPage('L', 'A4');

        #echo "<h1>Mulai TABLE</h1>";
        $_page = '
        <table>
        <tr><td align="center"><b>
        LAMPIRAN SURAT KEPUTUSAN<br />
        NO: '.$_sk->title.'/A2.1/KU/2018<br />
        TANGGAL : '.$tanggal.'<br /></b>
        </td></tr></table>
        ';
        #$pdf->writeHTML($html, true, false, true, false, '');
        $_page .= "<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\">\n";
        $_page .= "<tr>\n";
        $_page .= "<th width=\"5%\" align=\"center\"><b>No.</b></th>\n";
        $_page .= "<th width=\"35%\" align=\"center\"><b>NAMA DAN ALAMAT LEMBAGA / ORGANISASI</b></th>\n";
        $_page .= "<th width=\"20%\" align=\"center\"><b>NAMA BANK DAN NO REKENING</b></th>\n";
        $_page .= "<th width=\"20%\" align=\"center\"><b>NAMA PENERIMA</b></th>\n";
        $_page .= "<th width=\"20%\" align=\"center\"><b>JUMLAH PERSETUJUAN</b></th>\n";
        $_page .= "</tr>\n";
        #$_page .= "<tr><td colspan='5'>&nbsp;</td></tr>\n";
      }

      $prop = node_load($k);
      #echo 'Nomor: '.$counter."<br />\n";
      $_page .= "<tr>\n";
      $_page .= '<td>'.$counter."</td>\n";
      #echo 'NID Jenis Proposal: '.$prop->field_pro_jenis_proposal['und'][0]['tid']."<br />\n";
      #jika proposal perseorangan
      if ($prop->field_pro_jenis_proposal['und'][0]['tid'] === '494') {
        #echo 'NID Perseorangan: '.$prop->field_pro_perseorangan['und'][0]['target_id']."<br />\n";
        $instansi = node_load($prop->field_pro_perseorangan['und'][0]['target_id']);
        $nama_alamat = $instansi->title.'<br />'.nl2br($instansi->field_inv_alamat['und'][0]['value']);
        #echo 'Nama dan Alamat: '.$nama_alamat."<br />\n";
        $_page .= '<td>'.$nama_alamat."</td>";
        #echo 'NID Bank: '.$instansi->field_inv_bank_v2['und'][0]['target_id']."<br />\n";
        $_bank = taxonomy_term_load($instansi->field_inv_bank_v2['und'][0]['target_id']);
        $bank = $_bank->name;
        $bank_dan_rekening = $bank.'<br />'.$instansi->field_inv_cabang_bank['und'][0]['value'].'<br />'.$instansi->field_inv_no_rekening['und'][0]['value'];
        #echo 'Nama Bank dan No. Rekening: '.$bank_dan_rekening."<br />\n";
        $_page .= '<td>'.$bank_dan_rekening."</td>";
        #echo 'NAMA PENERIMA: '.$instansi->title."<br />\n";
        $_page .= '<td>'.$instansi->title."</td>";
      } else {
        #echo 'NID Lembaga: '.$prop->field_pro_lembaga['und'][0]['target_id']."<br />\n";
        $instansi = node_load($prop->field_pro_lembaga['und'][0]['target_id']);
        $nama_alamat = $instansi->title.'<br />'.nl2br($instansi->field_lem_alamat['und'][0]['value']);
        #echo 'Nama dan Alamat: '.$nama_alamat."<br />\n";
        $_page .= '<td>'.$nama_alamat."</td>";
        #echo 'NID Bank: '.$instansi->field_lem_bank_v2['und'][0]['target_id']."<br />\n";
        $_bank = taxonomy_term_load($instansi->field_lem_bank_v2['und'][0]['target_id']);
        $bank = $_bank->name;
        $bank_dan_rekening = $bank.'<br />'.$instansi->field_lem_cabang_bank['und'][0]['value'].'<br />'.$instansi->field_lem_no_rekening['und'][0]['value'];
        #echo 'Nama Bank dan No. Rekening: '.$bank_dan_rekening."<br />\n";
        $_page .= '<td>'.$bank_dan_rekening."</td>";
        #echo 'NAMA PENERIMA: '.$instansi->title."<br />\n";
        $_page .= '<td>'.$instansi->title."</td>";
      }
      #echo 'JUMLAH PERSETUJUAN: '.$prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']."<br />\n";
      $_page .= '<td>Rp'.number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, ".", ".")."</td>";
      #echo "<br />\n";
      $_page .= "</tr>\n";
      #$_page .= "<tr><td colspan='5'>&nbsp;</td></tr>\n";

      #if ( ($counter % 5) == 0) {
      if ((($counter % 5) == 0) AND ($counter  < $total_rec)) {
        $_page .= "</table>\n";

$pdf->writeHTML($_page, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();

      }

      if ($counter  == $total_rec) {
        #$_page .= "<tr><td></td><td colspan=\"3\">JUMLAH</td><td>Rp".number_format($_sk->field_sk_total['und'][0]['value'], 0, '.', '.')."</td></tr>\n";
        $_page .= "<tr><td></td><td colspan=\"3\">JUMLAH</td><td>Rp".$_total."</td></tr>\n";
        $_page .= "</table>\n";
        #$_page .= "<h1>Signature disini</h1>\n";


$_page .= '<br /><table width="700" class="sk_intro">';
$_page .= '<tr>';
$_page .= '  <td width="50%" align="center">';
$_page .= '    <br /><br />';
$_page .= '    Kuasa Pengguna Anggaran<br />';
$_page .= '    Kepala Biro Keuangan<br />';
$_page .= '    &nbsp;<br />';
$_page .= '    &nbsp;<br />';
#$_page .= '    &nbsp;<br />';
$_page .= '    &nbsp;<br />';
$_page .= '    <b>M.Q. Wisnu Aji</b><br />';
$_page .= '    NIP. 196005221986011001';
$_page .= '  </td>';
$_page .= '  <td width="50%" align="center">';
$_page .= '    &nbsp;<br />';
$_page .= '    Pejabat Pembuat Komitmen<br />';
$_page .= '    &nbsp;<br />';
$_page .= '    &nbsp;<br />';
$_page .= '    &nbsp;<br />';
#$_page .= '    &nbsp;<br />';
$_page .= '    &nbsp;<br />';
$_page .= '    <b>Taopiq</b><br />';
$_page .= '    NIP. 197703112000031002';
$_page .= '  </td>';
$_page .= '</tr>';
$_page .= '</table>';

$pdf->writeHTML($_page, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();

      }



    }




// print a block of text using Write()
#$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_002.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+



  } 

} else {
  die('Error parameter!');
}
?>


