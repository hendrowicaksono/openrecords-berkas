<?php

$base_url = '/simbanpem/cetak/';

if (!isset($_SERVER['PATH_INFO'])) {
  die('Bad URL');
}
$_path = $_SERVER['PATH_INFO'];

$path = explode('/', $_path);

if (is_numeric($path[1])) {
  require '/var/www/html/simbanpem/barcode/vendor/autoload.php';
  # import Drupal Environment
  define ('DRUPAL_ROOT', '/var/www/html/simbanpem');
  require_once DRUPAL_ROOT.'/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $_sk = node_load($path[1]);
  $_jenis_bantuan = taxonomy_term_load ($_sk->field_sk_jenis_bantuan['und'][0]['tid']);
  $_total = number_format($_sk->field_sk_total['und'][0]['value']);

  if (!$_sk) {
    die('Invalid Node ID');
  }
  if ($_sk->type != 'sk') {
    die('Invalid Node Type');
  }

  #cek apakah status sudah menjadi SK
  if ($_sk->field_sk_status['und'][0]['tid'] != '1099') {
    die('Not ready for SK printing.');
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'proposal')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('field_sk', 'target_id', $path[1], '=')
    ->fieldOrderBy('field_pro_nomor_pendaftaran', 'value', 'DESC')
    #->range(0, 10)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));

  $result = $query->execute();
  if (isset($result['node'])) {

###############################################


// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// extend TCPF with custom functions
class MYPDF extends TCPDF {

  // Load table data from file
  public function LoadData($file) {
    // Read file lines
    $lines = file($file);
    $data = array();
    foreach($lines as $line) {
      $data[] = explode(';', chop($line));
    }
    return $data;
  }

  // Colored table
  public function ColoredTable($header,$data) {
    // Colors, line width and bold font
    $this->SetFillColor(255, 0, 0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128, 0, 0);
    $this->SetLineWidth(0.3);
    $this->SetFont('', 'B');
    // Header
    $w = array(40, 35, 40, 45);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
      $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
    }
    $this->Ln();
    // Color and font restoration
    $this->SetFillColor(224, 235, 255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Data
    $fill = 0;
    foreach($data as $row) {
      $this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
      $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
      $this->Cell($w[2], 6, number_format($row[2]), 'LR', 0, 'R', $fill);
      $this->Cell($w[3], 6, number_format($row[3]), 'LR', 0, 'R', $fill);
      $this->Ln();
      $fill=!$fill;
    }
    $this->Cell(array_sum($w), 0, '', 'T');
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 011');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
#$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 011', PDF_HEADER_STRING);
$pdf->setPrintHeader(false);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, '15', PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
  require_once(dirname(__FILE__).'/lang/eng.php');
  $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 12);






    #echo count($result['node']); die();
    $total_rec = count($result['node']);

    $counter = 0;
    foreach ($result['node'] as $k => $v) {
      $counter++;

      if ( ($counter == 1) OR ($counter % 5 == 1) ) {

        // add a page
        #$pdf->AddPage();
        $pdf->AddPage('L', 'A4');

        #echo "<h1>Mulai TABLE</h1>";

$html = '
<table><tr><td align="center">HTML Example</td></tr></table>
';
$pdf->writeHTML($html, true, false, true, false, '');

// print colored table
#$pdf->ColoredTable($header, $data);
#################
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetLineWidth(0.3);
$pdf->SetFont('', 'B');
// Header
$w = array(40, 35, 40, 45);
$num_headers = count($header);
#for($i = 0; $i < $num_headers; ++$i) {
# $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
#}
$header_nama_dan_alamat = "
NAMA DAN ALAMAT &#13;
LEMBAGA/ORGANISASI\n
";
$pdf->Cell(10, 7, 'No', 1, 0, 'C', 1);
$pdf->Cell(50, 7, $header_nama_dan_alamat, 1, 0, 'C', 1);
$pdf->Cell(55, 7, 'Capital', 1, 0, 'C', 1);
$pdf->Cell(50, 7, 'Area', 1, 0, 'C', 1);
$pdf->Cell(55, 7, 'Pop', 1, 0, 'C', 1);

$pdf->Ln();
// Color and font restoration
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('');
// Data
$fill = 0;



      }

      $prop = node_load($k);
      #echo 'Nomor: '.$counter."<br />\n";
      $_page .= "<tr>\n";
      $_page .= '<td>'.$counter."</td>\n";
      #echo 'NID Jenis Proposal: '.$prop->field_pro_jenis_proposal['und'][0]['tid']."<br />\n";
      #jika proposal perseorangan
      if ($prop->field_pro_jenis_proposal['und'][0]['tid'] === '494') {
        #echo 'NID Perseorangan: '.$prop->field_pro_perseorangan['und'][0]['target_id']."<br />\n";
        $instansi = node_load($prop->field_pro_perseorangan['und'][0]['target_id']);
        $nama_alamat = $instansi->title.'<br />'.nl2br($instansi->field_inv_alamat['und'][0]['value']);
        #echo 'Nama dan Alamat: '.$nama_alamat."<br />\n";
        $_page .= '<td>'.$nama_alamat."</td>";
        #echo 'NID Bank: '.$instansi->field_inv_bank_v2['und'][0]['target_id']."<br />\n";
        $_bank = taxonomy_term_load($instansi->field_inv_bank_v2['und'][0]['target_id']);
        $bank = $_bank->name;
        $bank_dan_rekening = $bank.'<br />'.$instansi->field_inv_cabang_bank['und'][0]['value'].'<br />'.$instansi->field_inv_no_rekening['und'][0]['value'];
        #echo 'Nama Bank dan No. Rekening: '.$bank_dan_rekening."<br />\n";
        $_page .= '<td>'.$bank_dan_rekening."</td>";
        #echo 'NAMA PENERIMA: '.$instansi->title."<br />\n";
        $_page .= '<td>'.$instansi->title."</td>";

$pdf->Cell(10, 6, '1', 'LR', 0, 'L', $fill);
$pdf->Cell(50, 6, $nama_alamat, 'LR', 0, 'L', $fill);
$pdf->Cell(45, 6, $bank_dan_rekening, 'LR', 0, 'L', $fill);
$pdf->Cell(50, 6, $instansi->title, 'LR', 0, 'R', $fill);

$pdf->Cell(55, 6, number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, ".", "."), 'LR', 0, 'R', $fill);
$pdf->Ln();
$fill=!$fill;
#pdf}
#$pdf->Cell(170, 0, '', 'T');


      } else {
        #echo 'NID Lembaga: '.$prop->field_pro_lembaga['und'][0]['target_id']."<br />\n";
        $instansi = node_load($prop->field_pro_lembaga['und'][0]['target_id']);
        $nama_alamat = $instansi->title.'<br />'.nl2br($instansi->field_lem_alamat['und'][0]['value']);
        #echo 'Nama dan Alamat: '.$nama_alamat."<br />\n";
        $_page .= '<td>'.$nama_alamat."</td>";
        #echo 'NID Bank: '.$instansi->field_lem_bank_v2['und'][0]['target_id']."<br />\n";
        $_bank = taxonomy_term_load($instansi->field_lem_bank_v2['und'][0]['target_id']);
        $bank = $_bank->name;
        $bank_dan_rekening = $bank.'<br />'.$instansi->field_lem_cabang_bank['und'][0]['value'].'<br />'.$instansi->field_lem_no_rekening['und'][0]['value'];
        #echo 'Nama Bank dan No. Rekening: '.$bank_dan_rekening."<br />\n";
        $_page .= '<td>'.$bank_dan_rekening."</td>";
        #echo 'NAMA PENERIMA: '.$instansi->title."<br />\n";
        $_page .= '<td>'.$instansi->title."</td>";

$pdf->Cell(10, 6, '1', 'LR', 0, 'L', $fill);
$pdf->Cell(50, 6, $nama_alamat, 'LR', 0, 'L', $fill);
$pdf->Cell(45, 6, $bank_dan_rekening, 'LR', 0, 'L', $fill);
$pdf->Cell(50, 6, $instansi->title, 'LR', 0, 'R', $fill);

$pdf->Cell(55, 6, number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, ".", "."), 'LR', 0, 'R', $fill);
$pdf->Ln();
$fill=!$fill;
#pdf}
#$pdf->Cell(170, 0, '', 'T');

      }
      #echo 'JUMLAH PERSETUJUAN: '.$prop->field_pro_usulan_jumlah_bantuan['und'][0]['value']."<br />\n";
      $_page .= '<td>Rp'.number_format($prop->field_pro_usulan_jumlah_bantuan['und'][0]['value'], 0, ".", ".")."</td>";
      #echo "<br />\n";
      $_page .= "</tr>\n";

      if ( ($counter % 5) == 0) {
        #$_page .= "</table>\n";
$pdf->Cell(210, 0, '', 'T');


      }

      if ($counter  == $total_rec) {
        #$_page .= "</table>\n";
        #$_page .= "<h1>Signature disini</h1>\n";
$pdf->Cell(210, 0, '', 'T');



      }



    }




// print a block of text using Write()
#$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_002.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+



  } 

} else {
  die('Error parameter!');
}
?>


