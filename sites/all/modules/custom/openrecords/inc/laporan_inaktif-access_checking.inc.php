<?php
if (!defined('SECURE_CHECKING')) {
  die('cannot access file directly');
}

$info_laporan = node_load(arg(2));
global $user;
#global $base_path;
$user_detail = user_load($user->uid);
#var_dump($user_detail->field_user_unit_pengolah['und'][0]['target_id']);
#var_dump($info_laporan->field_unit_pengolah['und'][0]['target_id']);
#$inc_files = $base_path.'sites/all/modules/custom/openrecords/inc/';


### tolak akses jika dari admin unit kerja yang berbeda. #######
/**
if (in_array("admin unit kerja", $user->roles)) {
  if ($user_detail->field_user_unit_pengolah['und'][0]['target_id'] != $info_laporan->field_unit_pengolah['und'][0]['target_id']) {
    #drupal_set_message($user_detail->field_user_unit_pengolah['und'][0]['target_id']);
    #drupal_set_message($info_laporan->field_unit_pengolah['und'][0]['target_id']);
    drupal_set_message('Access not allowed.'); drupal_access_denied(); die();
  }
}
**/
  if ($user_detail->field_unit_kerja_v2['und'][0]['tid'] != $info_laporan->field_unit_kerja_v2['und'][0]['tid']) {
    #drupal_set_message($user_detail->field_user_unit_pengolah['und'][0]['target_id']);
    #drupal_set_message($info_laporan->field_unit_pengolah['und'][0]['target_id']);
    drupal_set_message('Access not allowed.'); drupal_access_denied(); die();
  }



################################################################
### hide daftar arsip yang siap untuk di retensi ###############
if ($info_laporan->field_status_laporan_inaktif['und'][0]['value'] == 1) {
  echo '<style>';
  echo '.view-dft-arsip-aktif-retensi {display: none;}';
  echo '</style>';
}
if ($info_laporan->field_status_laporan_inaktif['und'][0]['value'] == 1) {
  echo '<style>';
  echo '.view-daftar-data-arsip-per-admin-unit-retensi-aktif {display: none;}';
  echo '</style>';
}
#################################################################

### UPDATE LIST RETENSI ARSIP ###################################
if ( (isset($_POST['records_nid'])) AND (isset($_POST['records_action'])) ) {
  if (in_array("admin unit kerja", $user->roles)) {
    if ($user_detail->field_unit_kerja_v2['und'][0]['tid'] == $info_laporan->field_unit_kerja_v2['und'][0]['tid']) {
      if ($info_laporan->field_status_laporan_inaktif['und'][0]['value'] == 0) {
        add_records_to_retention_list_v2($_POST['records_nid'], $_POST['records_action'], arg(2));
      } else {
        drupal_set_message ('Berkas retensi sudah final. Anda tidak diperkenankan lagi menambah atau mengurangi berkas arsip.', 'warning');
      }
    } else {
      #debug
      drupal_set_message ('Method Not Allowed (2)');
    }
  } else {
    #debug
    drupal_set_message ('Method Not Allowed (1)');
  }
}
##################################################################

?>
