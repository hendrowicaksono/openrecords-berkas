<?php
global $user;

$loggedin_user = user_load($user->uid);
#echo 'yo ayo';
if (!defined('SECURE_CHECKING_RECORDS_DELETION')) {
  die('cannot access file directly');
}

if (!is_numeric(arg(1))) {
  drupal_set_message('Not numeric.', 'warning'); drupal_access_denied(); die();
}

$node2delete = node_load(arg(1));
if (!$node2delete) {
  drupal_not_found(); die();
}

if ($node2delete->type != 'data_arsip') {
  drupal_not_found(); die();
}

if ($node2delete->status == 0) {
  drupal_set_message('Item not available.');
  drupal_not_found(); die();
}

#var_dump($node2delete->field_laporan_inaktif['und']);
#cek apakah sudah masuk dalam daftar arsip inaktif
if (isset($node2delete->field_laporan_inaktif['und'])) {
  if (!is_null($node2delete->field_laporan_inaktif['und'][0]['target_id'])) {
    drupal_set_message('Data sudah terdaftar sebagai arsip inaktif. Untuk sementara tidak bisa dihapus.'); drupal_access_denied(); die();
  }
}


$_end = end($loggedin_user->field_unit_kerja_v2['und']);
#var_dump($_end['tid']);echo '<hr />';
#var_dump($node2delete->field_unit_kerja_v2['und']);
$is_allowed = FALSE;
foreach ($node2delete->field_unit_kerja_v2['und'] as $k => $v) {
  #drupal_set_message($v['tid']);
  if ($_end['tid'] == $v['tid']) {
    $is_allowed = TRUE;
  }
}
#DEBUG
#if ($is_allowed) {
#  drupal_set_message( 'anda punya hak delete');
#} else {
#  drupal_set_message( 'anda GA punya hak delete');
#}

if (!$is_allowed) {
  drupal_set_message('Not allowed.'); drupal_access_denied(); die();
}

#var_dump($node2delete->field_unit_pengolah);
#var_dump($loggedin_user->field_user_unit_pengolah);
#echo $base_path;

if (isset($_POST['delete'])) {
  drupal_set_message('akses hapus dilakukan');
  $node2delete->status = 0;
  node_save($node2delete);
  drupal_goto();
}

/**
$info_laporan = node_load(arg(2));
global $user;
#global $base_path;
$user_detail = user_load($user->uid);
#var_dump($user_detail->field_user_unit_pengolah['und'][0]['target_id']);
#var_dump($info_laporan->field_unit_pengolah['und'][0]['target_id']);
#$inc_files = $base_path.'sites/all/modules/custom/openrecords/inc/';

### tolak akses jika dari admin unit kerja yang berbeda. #######
if (in_array("admin unit kerja", $user->roles)) {
  if ($user_detail->field_user_unit_pengolah['und'][0]['target_id'] != $info_laporan->field_unit_pengolah['und'][0]['target_id']) {
    #drupal_set_message($user_detail->field_user_unit_pengolah['und'][0]['target_id']);
    #drupal_set_message($info_laporan->field_unit_pengolah['und'][0]['target_id']);

    drupal_set_message('Access not allowed.'); drupal_access_denied();
    die();
  }
}
################################################################
### hide daftar arsip yang siap untuk di retensi ###############
if ($info_laporan->field_status_laporan_inaktif['und'][0]['value'] == 1) {
  echo '<style>';
  echo '.view-dft-arsip-aktif-retensi {display: none;}';
  echo '</style>';
}
if ($info_laporan->field_status_laporan_inaktif['und'][0]['value'] == 1) {
  echo '<style>';
  echo '.view-daftar-data-arsip-per-admin-unit-retensi-aktif {display: none;}';
  echo '</style>';
}
#################################################################

### UPDATE LIST RETENSI ARSIP ###################################
if ( (isset($_POST['records_nid'])) AND (isset($_POST['records_action'])) ) {
  if (in_array("admin unit kerja", $user->roles)) {
    if ($user_detail->field_user_unit_pengolah['und'][0]['target_id'] == $info_laporan->field_unit_pengolah['und'][0]['target_id']) {
      add_records_to_retention_list_v2($_POST['records_nid'], $_POST['records_action'], arg(2));
    } else {
      #debug
      drupal_set_message ('Method Not Allowed (2)');
    }
  } else {
    #debug
    drupal_set_message ('Method Not Allowed (1)');
  }
}
##################################################################
**/
?>
<h1>Delete Records?</h1>
<h3>Nomor Arsip: <?php echo $node2delete->title; ?></h3>
<hr />
<div class="row">
  <div class="col-md-1">
    <form class="form-inline" method="post" action="<?php echo $base_path.'records/'.arg(1).'/delete'; ?>">
      <button name="delete" type="submit" class="btn btn-danger">Delete</button>
    </form>
  </div>
  <div class="col-md-11">
    <form class="form-inline" method="get" action="<?php echo $base_path; ?>">
      <button type="submit" class="btn btn-primary">Cancel</button>
    </form>
  </div>
</div>
