<?php
  print drupal_render($form['title']);
?>

<div class="panel panel-default">
  <div class="panel-body">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2"><h5>Master Type: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_master_type']); ?></div>
        <div class="col-md-2"><h5>Vendor Account No: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_vendor_account_no']); ?></div>
      </div>
      <div class="row">
        <div class="col-md-2"><h5>Sub type: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_sub_type']); ?></div>
        <div class="col-md-2"><h5>Box ID: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_box_id']); ?></div>
      </div>
      <div class="row">
        <div class="col-md-3" style="padding-top: 5px;"><?php print drupal_render($form['field_non_index_content']); ?></div>
        <div class="col-md-3" style="padding-top: 5px;"><?php print drupal_render($form['field_allow_check_out']); ?></div>
        <div class="col-md-2"><h5>Old Barcode: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_old_barcode']); ?></div>
      </div>
      <div class="row">
        <div class="col-md-2"><h5>Box Barcode: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_box_barcode']); ?></div>
        <div class="col-md-2"><h5>Archive Barcode: </h5></div>
        <div class="col-md-4"><?php print drupal_render($form['field_archive_barcode']); ?></div>
      </div>
    </div>
  </div>
</div>


<!--
<div>
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
  <li><a data-toggle="tab" href="#detail">Detail</a></li>
  <li><a data-toggle="tab" href="#standard">Standard</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>HOME</h3>
    <p>Some content.</p>
  </div>
  <div id="detail" class="tab-pane fade">
    <h3>Menu 1</h3>
    <p>Some content in menu 1.</p>
  </div>
  <div id="standard" class="tab-pane fade">
    <h3>Menu 2</h3>
    <p>Some content in menu 2.</p>
  </div>
</div>
</div>
-->

  <div class="container">


    <div id="content">
      <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#red" data-toggle="tab">Red</a>
        </li>
        <li><a href="#orange" data-toggle="tab">Orange</a>
        </li>
        <li><a href="#yellow" data-toggle="tab">Yellow</a>
        </li>
        <li><a href="#green" data-toggle="tab">Green</a>
        </li>
        <li><a href="#blue" data-toggle="tab">Blue</a>
        </li>
      </ul>
    </div>
    <div id="my-tab-content" class="tab-content">
      <div class="tab-pane active" id="red">
        <h1>Red</h1>
        <p>red red red red red red</p>
      </div>
      <div class="tab-pane" id="orange">
        <h1>Orange</h1>
        <p>orange orange orange orange orange</p>
      </div>
      <div class="tab-pane" id="yellow">
        <h1>Yellow</h1>
        <p>yellow yellow yellow yellow yellow</p>
      </div>
      <div class="tab-pane" id="green">
        <h1>Green</h1>
        <p>green green green green green</p>
      </div>
      <div class="tab-pane" id="blue">
        <h1>Blue</h1>
        <p>blue blue blue blue blue</p>
      </div>
    </div>
  </div>




  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

