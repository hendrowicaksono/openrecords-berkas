<?php
#referensi: https://www.drupal.org/node/1655012
function detail_rup_swakelola_perunitkerja($year) {
  $_return = '<ol>';
  $_uu = views_get_view('daftar_unit_utama');
  $_uu->execute();
  #var_dump($_uu->result);
  foreach ($_uu->result as $k_uu => $v_uu) {
    $unit_code = $_uu->render_field('field_tx_uu_unit_code', $k_uu);
    $unit_name = $_uu->render_field('name', $k_uu);
    $tid = $_uu->render_field('tid', $k_uu);
    #debug
    #echo $unit_code.' - '.$unit_name.'<br />';

    #RUP SWAKELOLA
    $_swk_total_pagu = 0;
    $_swk_total_kegiatan = 0;
    $_swk_ut = views_get_view('barebone_rup_swakelola');
    $_swk_ut->exposed_input['field_pkt_unit_kerja_value'] = $unit_name;
    $_swk_ut->execute();
    foreach ($_swk_ut->result as $k_swk_ut => $v_swk_ut) {
      #$kegiatan = $_swk_ut->render_field('field_pkt_kegiatan', $k_swk_ut);
      $kegiatan = $_swk_ut->render_field('field_pkt_kegiatan', $k_swk_ut);
      $pagu = $_swk_ut->render_field('field_swa_total_pagu_v2', $k_swk_ut);
      #$total_pagu = $total_pagu + $pagu;
      $_swk_total_pagu = $_swk_total_pagu + $pagu;
      $_swk_total_kegiatan++;
      $unit_kerja = $_swk_ut->render_field('field_pkt_unit_kerja', $k_swk_ut);
      #debug
      #echo '--- '.$kegiatan.' - '.$pagu.' - '.$unit_kerja.'<br />';
    }
    #$_swk_total_pagu = floatval($_swk_total_pagu);
    #debug
    #echo 'Total Keg & Pagu: '.$_swk_total_kegiatan.' - '.$_swk_total_pagu.'<br />';

    $upd_term = taxonomy_term_load($tid);
    $upd_term->field_tx_uu_total_kegiatan[LANGUAGE_NONE][2016]['value'] = $_swk_total_kegiatan; 
    $upd_term->field_tx_uu_total_pagu[LANGUAGE_NONE][2016]['value'] = $_swk_total_pagu; 
    taxonomy_term_save($upd_term);

    $node = new stdClass();
    $node->type = "statistik_unit_utama";
    node_object_prepare($node); // Sets some defaults. Invokes hook_prepare() and hook_node_prepare().

    $node->title = $unit_name;
    $node->language = LANGUAGE_NONE; // Or e.g. 'en' if locale is enabled

    $node->field_stu_unit_utama[$node->language][0]['target_id'] = $tid;
    $node->field_stu_jumlah_kegiatan[$node->language][0]['value'] = $_swk_total_kegiatan;
    $node->field_stu_jumlah_pagu[$node->language][0]['value'] = $_swk_total_pagu;
    $node->field_pkt_tahun[$node->language][0]['value'] = '2016';

    $node->uid = '1';
    $node->status = 1; //(1 or 0): published or not
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write
    $node = node_submit($node); // Prepare node for saving
    node_save($node);
    $_return .= '<li>Unit utama: '.$unit_name.'. Kegiatan: '.$_swk_total_kegiatan.'. Total pagu: '.$_swk_total_pagu.'.</li>';
    #echo 'Total Keg & Pagu: '.$_swk_total_kegiatan.' - '.$_swk_total_pagu.'<br />';
  }
  $_return .= '</ol>';
  #return 'Hello Universe!';
  return $_return;
}






















/**
  $_node = node_load ($nid);
  #var_dump($_node->field_lampiran);
  $_res = views_embed_view('detail_usulan_kegiatan', 'block_uk_nama_kegiatan', $nid);

  #$_res .= views_embed_view('detail_usulan_kegiatan', 'block_uk_struktur_organisasi', $nid);
  if (!empty($_node->field_struktur_organisasi)) {
    $_res .= '<div class="row">';
    $_res .= '  <div class="col-md-3"><h4>Struktur Organisasi</h4></div>';
    $_res .= '  <div class="col-md-9">';
    $_res .= '  <ol class="breadcrumb">';
    foreach ($_node->field_struktur_organisasi['und'] as $value) {
      $term = taxonomy_term_load($value['tid']);
       $_res .= '<li>'.$term->name.'</li>';
    }
    $_res .= '  </ol>';
    $_res .= '  </div>';
    $_res .= '</div>';
  }

  if (!empty($_node->field_lampiran)) {
    $_res .= '<div class="row">';
    $_res .= '  <div class="col-md-3"><h4>Lampiran</h4></div>';
    $_res .= '  <div class="col-md-9"><h4>';
    $_res .= views_embed_view('detail_usulan_kegiatan', 'block_uk_lampiran', $nid);
    $_res .= '  </h4></div>';
    $_res .= '</div>';
  }
**/
  #$_res .= '';
  #print views_embed_view('detail_usulan_kegiatan','block_1');


?>