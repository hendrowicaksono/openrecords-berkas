(function($) {
Drupal.behaviors.myBehavior = {
  attach: function (context, settings) {



  $(function() {
    $("#edit-field-renstra-uk-disusun-und").change(function() {
      $('#sakip_field_renstra_uk_disusun').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-memuat-tujuan-und").change(function() {
      $('#sakip_field_renstra_memuat_tujuan').html( $('option:selected', this).val() );
    });
    $("#edit-field-tujuan-indikator-und").change(function() {
      $('#sakip_field_tujuan_indikator').html( $('option:selected', this).val() );
    });
    $("#edit-field-target-keberhasilan-und").change(function() {
      $('#sakip_field_target_keberhasilan').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-sasaran-und").change(function() {
      $('#sakip_field_renstra_sasaran').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-indikator-und").change(function() {
      $('#sakip_field_renstra_indikator').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-target-und").change(function() {
      $('#sakip_field_renstra_target').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-menyajikan-und").change(function() {
      $('#sakip_field_renstra_menyajikan').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-dipublikasikan-und").change(function() {
      $('#sakip_field_renstra_dipublikasikan').html( $('option:selected', this).val() );
    });
    $("#edit-field-tujuan-berorientasi-hasil-und").change(function() {
      $('#sakip_field_tujuan_berorientasi_hasil').html( $('option:selected', this).val() );
    });
    $("#edit-field-ukuran-keberhasilan-und").change(function() {
      $('#sakip_field_ukuran_keberhasilan').html( $('option:selected', this).val() );
    });
    $("#edit-field-sasaran-berorientasi-hasil-und").change(function() {
      $('#sakip_field_sasaran_berorientasi_hasil').html( $('option:selected', this).val() );
    });
    $("#edit-field-indikator-kinerja-sasaran-und").change(function() {
      $('#sakip_field_indikator_kinerja_sasaran').html( $('option:selected', this).val() );
    });
    $("#edit-field-target-kinerja-ditetapkan-und").change(function() {
      $('#sakip_field_target_kinerja_ditetapkan').html( $('option:selected', this).val() );
    });
    $("#edit-field-program-mencapai-sasaran-und").change(function() {
      $('#sakip_field_program_mencapai_sasaran').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-selaras-kmenterian-und").change(function() {
      $('#sakip_field_renstra_selaras_kmenterian').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-telah-menetapkan-und").change(function() {
      $('#sakip_field_renstra_telah_menetapkan').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-acuan-dokumen-und").change(function() {
      $('#sakip_field_renstra_acuan_dokumen').html( $('option:selected', this).val() );
    });
    $("#edit-field-target-jangka-menengah-und").change(function() {
      $('#sakip_field_target_jangka_menengah').html( $('option:selected', this).val() );
    });
    $("#edit-field-renstra-direviu-und").change(function() {
      $('#sakip_field_renstra_direviu').html( $('option:selected', this).val() );
    });
    $("#edit-field-dokumen-perencanaan-und").change(function() {
      $('#sakip_field_dokumen_perencanaan').html( $('option:selected', this).val() );
    });
    $("#edit-field-perjanjian-kinerja-disusun-und").change(function() {
      $('#sakip_field_perjanjian_kinerja_disusun').html( $('option:selected', this).val() );
    });
    $("#edit-field-menyajikan-indikator-und").change(function() {
      $('#sakip_field_menyajikan_indikator').html( $('option:selected', this).val() );
    });
    $("#edit-field-pk-telah-dipublikasikan-und").change(function() {
      $('#sakip_field_pk_telah_dipublikasikan').html( $('option:selected', this).val() );
    });
    $("#edit-field-rencana-aksi-atas-kinerja-und").change(function() {
      $('#sakip_field_rencana_aksi_atas_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-sasaran-telah-berorientasi-und").change(function() {
      $('#sakip_field_sasaran_telah_berorientasi').html( $('option:selected', this).val() );
    });
    $("#edit-field-indikator-kinerja-sasaran2-und").change(function() {
      $('#sakip_field_indikator_kinerja_sasaran2').html( $('option:selected', this).val() );
    });
    $("#edit-field-target-kinerja-ditetapkan2-und").change(function() {
      $('#sakip_field_target_kinerja_ditetapkan2').html( $('option:selected', this).val() );
    });
    $("#edit-field-kegiatan-merupakan-cara-und").change(function() {
      $('#sakip_field_kegiatan_merupakan_cara').html( $('option:selected', this).val() );
    });
    $("#edit-field-dokumen-rencana-kinerja-und").change(function() {
      $('#sakip_field_dokumen_rencana_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-dokumen-pk-telah-selaras-und").change(function() {
      $('#sakip_field_dokumen_pk_telah_selaras').html( $('option:selected', this).val() );
    });
    $("#edit-field-dokumen-pk-menetapkan-und").change(function() {
      $('#sakip_field_dokumen_pk_menetapkan').html( $('option:selected', this).val() );
    });
    $("#edit-field-rencana-aksi-atas-kinerja2-und").change(function() {
      $('#sakip_field_rencana_aksi_atas_kinerja2').html( $('option:selected', this).val() );
    });
    $("#edit-field-rencana-aksi-atas-kinerja3-und").change(function() {
      $('#sakip_field_rencana_aksi_atas_kinerja3').html( $('option:selected', this).val() );
    });
    $("#edit-field-rencana-kinerja-tahunan-und").change(function() {
      $('#sakip_field_rencana_kinerja_tahunan').html( $('option:selected', this).val() );
    });
    $("#edit-field-target-kinerja-dperjanjikn-und").change(function() {
      $('#sakip_field_target_kinerja_dperjanjikn').html( $('option:selected', this).val() );
    });
    $("#edit-field-rencana-aksi-atas-kinerja4-und").change(function() {
      $('#sakip_field_rencana_aksi_atas_kinerja4').html( $('option:selected', this).val() );
    });
    $("#edit-field-rencana-aksi-dimanfaatkan-und").change(function() {
      $('#sakip_field_rencana_aksi_dimanfaatkan').html( $('option:selected', this).val() );
    });
    $("#edit-field-perjanjian-kinerja-telah-und").change(function() {
      $('#sakip_field_perjanjian_kinerja_telah').html( $('option:selected', this).val() );
    });

    $("#edit-field-terdapat-indikator-kinerja-und").change(function() {
      $('#sakip_field_terdapat_indikator_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-indikator-kinerja-atasan-und").change(function() {
      $('#sakip_field_indikator_kinerja_atasan').html( $('option:selected', this).val() );
    });
    $("#edit-field-mekanisme-pengumpulan-data-und").change(function() {
      $('#sakip_field_mekanisme_pengumpulan_data').html( $('option:selected', this).val() );
    });
    $("#edit-field-indikator-dipublikasikan-und").change(function() {
      $('#sakip_field_indikator_dipublikasikan').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-ii-5-indikator-kinerja-und").change(function() {
      $('#sakip_field_b_ii_5_indikator_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-ii-6-indikator-kinerja-und").change(function() {
      $('#sakip_field_b_ii_6_indikator_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-ii-7-indikator-kinerja-und").change(function() {
      $('#sakip_field_b_ii_7_indikator_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-ii-8-indikator-kinerja-und").change(function() {
      $('#sakip_field_b_ii_8_indikator_kinerja').html( $('option:selected', this).val() );
    });

    $("#edit-field-b-ii-9-pengukuran-kinerja-und").change(function() {
      $('#sakip_field_b_ii_9_pengukuran_kinerja').html( $('option:selected', this).val() );
    });

    $("#edit-field-b-ii-10-pengumpulan-data-und").change(function() {
      $('#sakip_field_b_ii_10_pengumpulan_data').html( $('option:selected', this).val() );
    });

    $("#edit-field-b-ii-11-pengumpulan-data-und").change(function() {
      $('#sakip_field_b_ii_11_pengumpulan_data').html( $('option:selected', this).val() );
    });

    $("#edit-field-b-ii-12-pengukuran-kinerja-und").change(function() {
      $('#sakip_field_b_ii_12_pengukuran_kinerja').html( $('option:selected', this).val() );
    });

    $("#edit-field-b-iii-13-impl-pengukuran-und").change(function() {
      $('#sakip_field_b_iii_13_impl_pengukuran').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-iii-14-impl-pengukuran-und").change(function() {
      $('#sakip_field_b_iii_14_impl_pengukuran').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-iii-15-impl-pengukuran-und").change(function() {
      $('#sakip_field_b_iii_15_impl_pengukuran').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-iii-16-impl-pengukuran-und").change(function() {
      $('#sakip_field_b_iii_16_impl_pengukuran').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-iii-17-impl-pengukuran-und").change(function() {
      $('#sakip_field_b_iii_17_impl_pengukuran').html( $('option:selected', this).val() );
    });
    $("#edit-field-b-iii-18-impl-pengukuran-und").change(function() {
      $('#sakip_field_b_iii_18_impl_pengukuran').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-i-1-pelaporan-kinerja-und").change(function() {
      $('#sakip_field_c_i_1_pelaporan_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-i-2-pelaporan-kinerja-und").change(function() {
      $('#sakip_field_c_i_2_pelaporan_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-i-3-pelaporan-kinerja-und").change(function() {
      $('#sakip_field_c_i_3_pelaporan_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-i-4-pelaporan-kinerja-und").change(function() {
      $('#sakip_field_c_i_4_pelaporan_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-5-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_5_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-6-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_6_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-7-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_7_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-8-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_8_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-9-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_9_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-10-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_10_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-ii-11-informasi-kinerja-und").change(function() {
      $('#sakip_field_c_ii_11_informasi_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-iii-12-pemanfaatan-info-und").change(function() {
      $('#sakip_field_c_iii_12_pemanfaatan_info').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-iii-13-pemanfaatan-info-und").change(function() {
      $('#sakip_field_c_iii_13_pemanfaatan_info').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-iii-14-pemanfaatan-info-und").change(function() {
      $('#sakip_field_c_iii_14_pemanfaatan_info').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-iii-15-pemanfaatan-info-und").change(function() {
      $('#sakip_field_c_iii_15_pemanfaatan_info').html( $('option:selected', this).val() );
    });
    $("#edit-field-c-iii-16-pemanfaatan-info-und").change(function() {
      $('#sakip_field_c_iii_16_pemanfaatan_info').html( $('option:selected', this).val() );
    });

    $("#edit-field-d-i-1-eval-kinerja-und").change(function() {
      $('#sakip_field_d_i_1_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-i-2-eval-kinerja-und").change(function() {
      $('#sakip_field_d_i_2_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-i-3-eval-kinerja-und").change(function() {
      $('#sakip_field_d_i_3_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-i-4-eval-kinerja-und").change(function() {
      $('#sakip_field_d_i_4_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-5-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_5_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-6-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_6_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-7-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_7_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-8-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_8_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-9-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_9_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-10-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_10_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-ii-11-eval-kinerja-und").change(function() {
      $('#sakip_field_d_ii_11_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-iii-12-eval-kinerja-und").change(function() {
      $('#sakip_field_d_iii_12_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-d-iii-13-eval-kinerja-und").change(function() {
      $('#sakip_field_d_iii_13_eval_kinerja').html( $('option:selected', this).val() );
    });
    $("#edit-field-e-1-capai-sasaran-und").change(function() {
      $('#sakip_field_e_1_capai_sasaran').html( $('option:selected', this).val() );
    });
    $("#edit-field-e-2-capai-sasaran-und").change(function() {
      $('#sakip_field_e_2_capai_sasaran').html( $('option:selected', this).val() );
    });
    $("#edit-field-e-3-capai-sasaran-und").change(function() {
      $('#sakip_field_e_3_capai_sasaran').html( $('option:selected', this).val() );
    });


/**
    $("#edit-field-tahun-und").change(function() {
      $('#sakip_tahun_penilaian').html( $('option:selected', this).val() );
    });
**/

    /**$('#edit-field-tahun-und').on('change focus', function() {**/
    $('#edit-field-tahun-und').change(function() {
      $.post("http://localhost/rokeu/web/penilaian-sakip.php", {
        //vendor: /\(([0-9])\w+\)$/i
        year: this.value,
        _unit: $('#unit').val()
        //name: "Donald Duck",
        //city: "Duckburg"
      },
      function(data, status){
        document.getElementById("sakip_tahun_penilaian").innerHTML = data;
      });
    })

  });



//  $(function() {
//    $("#edit-field-renstra-uk-disusun-und").change(function() {
//      $('#sakip_field_renstra_uk_disusun').html( $('option:selected', this).val() );
//    });

//    $('.sakip_field_renstra_field select').change(function() {
//      var sum = 0;
//      $('.nilai').each(function(n) {
//        sum += parseInt($(this).text());
//      });
//      $('#renstra_total').html(sum);
//    });
//  });




  }
};
})(jQuery);